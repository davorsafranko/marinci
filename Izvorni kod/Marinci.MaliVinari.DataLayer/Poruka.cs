//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Marinci.MaliVinari.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class Poruka
    {
        public int idPoruka { get; set; }
        public int idRazgovor { get; set; }
        public int idKorisnik { get; set; }
        public System.DateTime Vrijeme { get; set; }
        public string Sadrzaj { get; set; }
    
        public virtual Razgovor Razgovor { get; set; }
    }
}
