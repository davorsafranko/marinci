﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class ApplicationParametersRepository : AbstractDataRepository<Parametri_aplikacije>
    {
        public void InsertOrUpdate(Parametri_aplikacije parametriAplikacije)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result = context.Parametri_aplikacije.FirstOrDefault(t => t.idKorisnik == parametriAplikacije.idKorisnik);
                if (result == null)
                {
                    context.Parametri_aplikacije.Add(parametriAplikacije);
                }
                else
                {
                    result.Boja_Skin = parametriAplikacije.Boja_Skin;
                    result.IP_Adresa = parametriAplikacije.IP_Adresa;
                    result.Port = parametriAplikacije.Port;
                }
                context.SaveChanges();
            }
        }

        public override void Update(Parametri_aplikacije entity)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Parametri_aplikacije entity)
        {
            throw new NotImplementedException();
        }

        public override Parametri_aplikacije GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Parametri_aplikacije GetParametersByUserId(int userId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Parametri_aplikacije.FirstOrDefault(t => t.idKorisnik == userId);
            }
        }
    }
}
