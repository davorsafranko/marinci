﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.DataLayer.Repositories
{
    public abstract class AbstractDataRepository<DLM> : IRepository<DLM> where DLM : class
    {
        public DLM Insert(DLM entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                context.Set<DLM>().Add(entity);
                context.SaveChanges();
                return entity;
            }
        }

        public abstract void Update(DLM entity);

        public abstract void Delete(DLM entity);

        public IEnumerable<DLM> GetAll()
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Set<DLM>().ToList();
            }
        }

        public abstract DLM GetById(int id);
    }
}
