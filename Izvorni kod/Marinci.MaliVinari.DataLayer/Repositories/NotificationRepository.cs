﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class NotificationRepository : AbstractDataRepository<Obavijest>
    {
        public override void Update(Obavijest entity)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Obavijest entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var notification = context.Obavijest.First(n => n.idObavijest == entity.idObavijest);
                if (notification != null)
                {
                    context.Obavijest.Remove(notification);
                }
            }
        }

        public override Obavijest GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Obavijest.First(n => n.idObavijest == id);
            }
        }

        public List<Obavijest> GetUsersNotifications(int userId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Obavijest.Where(o => (o.idKorisnik == userId) && (o.Je_Procitana == false)).ToList();
            }
        }

        public void SetNotificationRead(int notificationId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result = context.Obavijest.First(n => n.idObavijest == notificationId);
                result.Je_Procitana = true;
                context.SaveChanges();
            }
        }
    }
}
