﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class ConversationRepository : AbstractDataRepository<Razgovor>
    {
        public override void Update(Razgovor entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                Razgovor conversation = context.Razgovor.First(c => c.idRazgovor == entity.idRazgovor);
                if (conversation == null) { return; }
                conversation.Vrijeme_Pocetka = entity.Vrijeme_Pocetka;
                conversation.Vrijeme_Zavrsetka = entity.Vrijeme_Zavrsetka;
                context.SaveChanges();
            }
        }

        public override void Delete(Razgovor entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var conversation = context.Razgovor.First(c => c.idRazgovor == entity.idRazgovor);
                if (conversation != null)
                {
                    context.Razgovor.Remove(conversation);
                    context.SaveChanges();
                }
            }
        }

        public override Razgovor GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Razgovor.First(c => c.idRazgovor == id);
            }
        }

        public IEnumerable<Razgovor> GetGroupConversations(int groupId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Razgovor.Where(c => c.idGrupa == groupId && c.Vrijeme_Zavrsetka != null).OrderByDescending(c => c.Vrijeme_Zavrsetka).ToList();
            }
        }

        public Razgovor GetLastGroupConversation(int groupId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result =
                    context.Razgovor.Where(c => c.idGrupa == groupId && c.Vrijeme_Zavrsetka == null)
                        .OrderByDescending(c => c.Vrijeme_Zavrsetka)
                        .FirstOrDefault();

                return result;
            }
        }
    }
}
