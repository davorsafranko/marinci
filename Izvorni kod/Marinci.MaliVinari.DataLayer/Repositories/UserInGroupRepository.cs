﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class UserInGroupRepository : AbstractDataRepository<Korisnik_u_grupi>
    {
        public override void Update(Korisnik_u_grupi entity)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Korisnik_u_grupi entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var userInGroup = context.Korisnik_u_grupi.First(uig => uig.idKorisnikUGrupi == entity.idKorisnikUGrupi);
                if (userInGroup != null)
                {
                    context.Korisnik_u_grupi.Remove(userInGroup);
                    context.SaveChanges();
                }
            }
        }

        public override Korisnik_u_grupi GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik_u_grupi.First(uig => uig.idKorisnikUGrupi == id);
            }
        }

        public List<int> GetGroupUserIds(int groupId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik_u_grupi.Where(u => u.idGrupa == groupId).Select(u => u.idKorisnik).ToList();
            }
        }

        public Korisnik_u_grupi GetKorisnikUGrupi(int userId, int groupId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik_u_grupi.First(u => u.idKorisnik == userId && u.idGrupa == groupId);
            }
        }

        public IEnumerable<int> GetUserInGroups(int userId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik_u_grupi.Where(t => t.idKorisnik == userId).Select(t => t.idGrupa).ToList();
            }
        } 
    }
}
