﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class MessageRepository : AbstractDataRepository<Poruka>
    {
        public override void Update(Poruka entity)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Poruka entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var message = context.Poruka.First(m => m.idPoruka == entity.idPoruka);
                if (message != null)
                {
                    context.Poruka.Remove(message);
                }
            }
        }

        public override Poruka GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Poruka.First(m => m.idPoruka == id);
            }
        }

        public IEnumerable<Poruka> GetConversationMessages(int convoId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Poruka.Where(m => m.idRazgovor == convoId).OrderBy(m => m.Vrijeme).ToList();
            }
        }
    }
}
