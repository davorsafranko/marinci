﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class BlockedUsersRepository : AbstractDataRepository<Blokirani>
    {
        public override void Update(Blokirani entity)
        {
            throw new NotImplementedException();
        }

        public override void Delete(Blokirani entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var block = context.Blokirani.First(b => b.idBlokada == entity.idBlokada);
                if (block != null)
                {
                    context.Blokirani.Remove(block);
                    context.SaveChanges();
                }
            }
        }

        public override Blokirani GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Blokirani.First(b => b.idBlokada == id);
            }
        }

        public Blokirani GetBlockedRecord(int blockingUserId, int blockedUserId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result =
                    context.Blokirani.FirstOrDefault(
                        b => b.idKorisnik == blockingUserId && b.idBlokiranogKorisnika == blockedUserId);
                return result;
            }
        }

        public void DeleteByUserId(int userId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result = context.Blokirani.Where(t => t.idBlokiranogKorisnika == userId || t.idKorisnik == userId);
                context.Blokirani.RemoveRange(result);
                context.SaveChanges();
            }
        }
    }
}
