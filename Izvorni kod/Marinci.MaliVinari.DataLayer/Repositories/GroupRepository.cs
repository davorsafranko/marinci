﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class GroupRepository : AbstractDataRepository<Grupa>
    {
        public override void Update(Grupa entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result = context.Grupa.First(g => g.idGrupa == entity.idGrupa);
                result.Kreirana = entity.Kreirana;
                context.SaveChanges();
            }
        }

        public override void Delete(Grupa entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var group = context.Grupa.First(g => g.idGrupa == entity.idGrupa);
                if (group != null)
                {
                    context.Grupa.Remove(group);
                    context.SaveChanges();
                }
            }
        }

        public override Grupa GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Grupa.First(g => g.idGrupa == id);
            }
        }

        public IEnumerable<Grupa> GetUserGroups(int userId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var user = context.Korisnik.First(u => u.idKorisnik == userId);
                if (user != null)
                {
                    return user.Korisnik_u_grupi.Select(kug => kug.Grupa).ToList();
                }
                else
                {
                    return new List<Grupa>();
                }
            }
        }
    }
}
