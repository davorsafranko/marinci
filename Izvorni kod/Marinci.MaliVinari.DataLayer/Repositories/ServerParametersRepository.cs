﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.DataLayer
{
    public class ServerParametersRepository
    {
        public void InsertOrUpdate(Parametri_posluzitelja entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result = context.Parametri_posluzitelja.FirstOrDefault();

                if (result != null)
                {
                    result.Port = entity.Port;
                    result.Max_Korisnika = entity.Max_Korisnika;
                }
                else
                {
                    context.Parametri_posluzitelja.Add(entity);
                }
                context.SaveChanges();
            }
        }

        public Parametri_posluzitelja GetServerParameters()
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                if (context.Parametri_posluzitelja.Count() != 0)
                {
                    return context.Parametri_posluzitelja.First();
                }
                return null;
            }
        }
    }
}
