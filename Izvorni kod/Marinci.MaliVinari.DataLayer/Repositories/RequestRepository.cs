﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class RequestRepository : AbstractDataRepository<Zahtjev>
    {
        public override void Update(Zahtjev entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result = context.Zahtjev.First(r => r.idZahtjev == entity.idZahtjev);
                result.Odgovoren = entity.Odgovoren;
                result.Prihvacen = entity.Prihvacen;
                context.SaveChanges();
            }
        }

        public override void Delete(Zahtjev entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var request = context.Zahtjev.First(r => r.idZahtjev == entity.idZahtjev);
                if (request != null)
                {
                    context.Zahtjev.Remove(request);
                }
            }
        }

        public override Zahtjev GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Zahtjev.First(r => r.idZahtjev == id);
            }
        }


        /// <param name="groupId"></param>
        /// <param name="type"></param>
        /// <returns>Requests of specified group(groupId) of type(type).</returns>
        public IEnumerable<Zahtjev> GetRequestsForGroupOfType(int groupId, int type)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Zahtjev.Where(r => (r.idGrupa == groupId && r.Tip_Zahtjeva == type)).ToList();
            }
        }

        public List<Zahtjev> GetUsersRequests(int userId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Zahtjev.Where(r => (r.idKorisnik == userId) && r.Odgovoren == false).ToList();
            }
        }

        public void DeleteRequests(List<Zahtjev> requests)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                foreach (Zahtjev request in requests)
                {
                    var result = context.Zahtjev.First(r => r.idZahtjev == request.idZahtjev);
                    context.Zahtjev.Remove(result);
                }
                context.SaveChanges();
            }
        }
    }
}
