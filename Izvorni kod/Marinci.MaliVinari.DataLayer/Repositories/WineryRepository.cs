﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class WineryRepository : AbstractDataRepository<Vinarija>
    {
        public override void Update(Vinarija entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var vinarija = context.Vinarija.First(v => v.idVinarija == entity.idVinarija);
                if (vinarija != null)
                {
                    vinarija.Naziv_Vinarije = entity.Naziv_Vinarije;
                    vinarija.Adresa_Vinarije = entity.Adresa_Vinarije;
                    context.SaveChanges();
                }
            }
        }

        public override void Delete(Vinarija entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var vinarija = context.Vinarija.First(v => v.idVinarija == entity.idVinarija);
                if (vinarija != null)
                {
                    context.Vinarija.Remove(vinarija);
                    context.SaveChanges();
                }
            }
        }

        public override Vinarija GetById(int id)
        {
            Vinarija vinarija = null;
            using (var context = new MaliVinariEntitiesContext())
            {
                vinarija = context.Vinarija.First(v => v.idVinarija == id);
            }
            return vinarija;
        }

        public Vinarija GetUserWinery(int userId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Vinarija.FirstOrDefault(v => v.idKorisnik == userId);
            }
        }
    }
}
