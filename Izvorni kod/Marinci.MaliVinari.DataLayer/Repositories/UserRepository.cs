﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

using _BCrypt = BCrypt.Net.BCrypt;

namespace Marinci.MaliVinari.DataLayer
{
    public class UserRepository : AbstractDataRepository<Korisnik>
    {
        public override void Update(Korisnik entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var result = context.Korisnik.First(t => t.idKorisnik == entity.idKorisnik);
                if (result != null)
                {
                    if (entity.Lozinka != result.Lozinka)
                    {
                        result.Lozinka = _BCrypt.HashPassword(entity.Lozinka, 8);
                    }
                    result.Ime = entity.Ime;
                    result.Prezime = entity.Prezime;
                    result.Telefon = entity.Telefon;
                    result.Osobni_Status = entity.Osobni_Status;
                    result.Profilna_Slika = entity.Profilna_Slika;
                    result.Prijavljen = entity.Prijavljen;
                    context.SaveChanges();
                } 
            }
        }

        public override void Delete(Korisnik entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var user = context.Korisnik.First(t => t.idKorisnik == entity.idKorisnik);
                if (user != null)
                {
                    context.Korisnik.Remove(user);
                    context.SaveChanges();
                }
                
            }
        }

        public override Korisnik GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik.First(u => u.idKorisnik == id);
            }
        }



        public Korisnik GetUserByUserName(string username)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik.FirstOrDefault(t => t.Korisnicko_Ime == username);
            }
        }

        public IEnumerable<Korisnik> GetGroupMembers(int groupId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik_u_grupi.Where(uig => uig.idGrupa == groupId).Select(uig => uig.Korisnik).ToList();
            }
        }

        public Korisnik GetByEmail(string email)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik.FirstOrDefault(u => u.Email == email);
            }
        }

        public List<Korisnik> GetAllWinemakers(int skipUserId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik.Where(u => u.idUloga == 0 && u.idKorisnik != skipUserId).ToList();
            }
        }

        public IEnumerable<Korisnik> Search(string term)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Korisnik.Where(
                    u => (  u.Ime.Contains(term) ||
                             u.Prezime.Contains(term) ||
                             ((u.Vinarija.FirstOrDefault() != null) && u.Vinarija.FirstOrDefault().Vino.Any(w => w.Naziv_Vina.Contains(term)))   ))
                    .ToList();
            }
        }
        
        public IEnumerable<Korisnik> GetBlockedUsers(int userId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Blokirani.Where(b => b.idKorisnik == userId).Select(b => b.Korisnik).ToList();
            }
        }

        public void SetUserLoggedInStatus(int userId, bool loggedIn)
        {
            if (userId != -1)
            {
                using (var context = new MaliVinariEntitiesContext())
                {
                    var user = context.Korisnik.First(u => u.idKorisnik == userId);
                    user.Prijavljen = loggedIn;
                    context.SaveChanges();
                }
            }
        }

        public bool CheckAdminExistence()
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var user = context.Korisnik.Count(u => u.idUloga == 1);
                return user > 0;
            }
        }
    }
}
