﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.DataLayer.Repositories;

namespace Marinci.MaliVinari.DataLayer
{
    public class WineRepository : AbstractDataRepository<Vino>
    {
        public override void Update(Vino entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var vino = context.Vino.First(v => v.idVino == entity.idVino);
                if (vino != null)
                {
                    vino.Naziv_Vina = entity.Naziv_Vina;
                    vino.Slika_Vina = entity.Slika_Vina;
                    context.SaveChanges();
                }
            }
        }

        public override void Delete(Vino entity)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var vino = context.Vino.First(v => v.idVino == entity.idVino);
                if (vino != null)
                {
                    context.Vino.Remove(vino);
                    context.SaveChanges();
                }
            }
        }

        public override Vino GetById(int id)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                return context.Vino.First(w => w.idVino == id);
            }
        }

        public IEnumerable<Vino> GetWineryWines(int wineryId)
        {
            using (var context = new MaliVinariEntitiesContext())
            {
                var winery = context.Vinarija.First(w => w.idVinarija == wineryId);
                if (winery != null)
                {
                    return winery.Vino.ToList();
                }
                else
                {
                    return new List<Vino>();
                }
            }
        }
    }
}
