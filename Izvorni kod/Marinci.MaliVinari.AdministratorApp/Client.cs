﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Common;
using Marinci.MaliVinari.Business.Models;
using Newtonsoft.Json;

namespace Marinci.MaliVinari.AdministratorApp
{
    public class Client
    {
        private volatile IPEndPoint _serverIpEndPoint;
        private const int BufferSize = 65536;

        public Client(string serverIpAddress, int port)
        {
            _serverIpEndPoint = new IPEndPoint(IPAddress.Parse(serverIpAddress), port);
        }

        public void SendRequest(ServerRequest serverRequest)
        {
            Task.Run(() => SendRequestAsync(serverRequest));
        }

        public void SendRequestAsync(ServerRequest serverRequest)
        {
            try
            {
                byte[] request = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(serverRequest));

                Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Connect(_serverIpEndPoint);

                int sent = serverSocket.Send(request);

                serverSocket.Close();
            }
            catch (SocketException ex)
            {
                serverRequest.ServerRequestType = ServerRequestType.ServerNotAvailable;
            }
        }
    }
}
