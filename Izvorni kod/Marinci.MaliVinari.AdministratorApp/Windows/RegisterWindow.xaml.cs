﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Application = System.Windows.Application;

namespace Marinci.MaliVinari.AdministratorApp.Windows
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        private LoginWindow _loginWindow;
        private User _user = null;
        private Client _client;

        public RegisterWindow(LoginWindow loginWindow, Client client)
        {
            _loginWindow = loginWindow;
            InitializeComponent();
            Closing += ShutDown;
            _client = client;
            TextBlockWarning.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            _loginWindow.Show();
            _loginWindow.Refresh();
            this.Hide();
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            TextBlockWarning.Text = "";
            TextBlockWarning.Foreground = new SolidColorBrush(Colors.Red);
            try
            {
                RegistrationUser user = new RegistrationUser();
                user.UserName = TxtBoxUsername.Text;
                user.Email = TextBoxEmail.Text;
                user.Password = PasswordBox.Password;

                user.RoleId = 1;

                user.Validate();

                User admin = user.CreateUser();

                UserRepository userRepository = new UserRepository();
                userRepository.Insert(admin);

                TextBlockWarning.Foreground = new SolidColorBrush(Colors.Green);
                TextBlockWarning.Text = "Administrator uspješno dodan!";
               
                }
            catch (ValidationException exception)
            {
                TextBlockWarning.Text = "Podaci nisu valjani";
            }
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (!String.IsNullOrEmpty(serverRequest.Message))
                {
                    TextBlockWarning.Text = serverRequest.Message;
                }
                else
                {
                    TextBlockWarning.Text = "Registracija je uspješna!";
                    TextBlockWarning.Foreground = new SolidColorBrush(Colors.Green);
                }
            }));
        }

        private void ShutDown(object sender, CancelEventArgs e)
        {
            _loginWindow.Close();
            Application.Current.Shutdown();
        }
    }
}
