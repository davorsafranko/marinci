﻿using System;
using System.ComponentModel;
using System.Windows;
using Marinci.MaliVinari.Business;

namespace Marinci.MaliVinari.AdministratorApp.Windows
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private User _user = null;
        private Client _client;
        private UserRepository user;

        public LoginWindow()
        {
            InitializeComponent();

            user = new UserRepository();
            Refresh();

            this.Closing += ShutDown;

            //hardkodiramo IP adresu servera, svako kod sebe mora kod testiranja ovo promijeniti
            _client = new Client("10.129.47.135", 50001);
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            Window regWindow = new RegisterWindow(this, _client);
            regWindow.Visibility = Visibility.Visible;
            this.Hide();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            TextBlockWarning.Text = "";

            string username = TextBoxUsername.Text;
            string password = PasswordBox.Password;

            if (username.Equals("") || password.Equals(""))
            {
                TextBlockWarning.Text = "Morate unijeti i korisničko ime i lozinku!";
                return;
            }

            UserRepository user = new UserRepository();
            _user = user.GetUserByUserName(username);

            if(_user == null)
            {
                TextBlockWarning.Text = "Ne postoji korisnik sa zadanim korisničkim imenom!";
            }

            else if(_user.RoleId != 1)
            {
                TextBlockWarning.Text = "Korisnik mora biti administrator!";
            }

            else if(!_user.validPassword(password))
            {
                TextBlockWarning.Text = "Lozinka nije točna!";
            }
            else
            {
                Window Interface = new MainWindow(this, _user.Id, _client);
                Interface.Show();

                this.Hide();
            }
        }

        private void ShutDown(object sender, CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        public void Refresh()
        {
            if (user.CheckAdminExistence())
            {
                BtnRegister.Visibility = Visibility.Hidden;
            }
        }
    }
}
