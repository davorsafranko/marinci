﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using System.ComponentModel;

namespace Marinci.MaliVinari.AdministratorApp.Windows
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<User> _users;
        private UserRepository _userRepo;
        private int _adminId;
        private Client _client;
        private Window _loginWindow;
        private BlockedUsersRepository _blockedUsersRepository;

        public MainWindow(Window loginWindow, int adminId, Client client)
        {
            InitializeComponent();
            _adminId = adminId;
            _userRepo = new UserRepository();
            _blockedUsersRepository = new BlockedUsersRepository();
            _users = _userRepo.GetAllWinemakers(_adminId).ToList();
            ListViewWiners.ItemsSource = _users;
            _client = client;
            _loginWindow = loginWindow;
            this.Closing += ShutDown;
        }

        private void BtnChangeParameters_Click(object sender, RoutedEventArgs e)
        {
            TextBlockError.Foreground = new SolidColorBrush(Colors.Red);
            TextBlockError.Text = "";
            string TextPort = TextBoxPort.Text;
            string TextMaxKorisnika = TextBoxMaxKorisnika.Text;
            if(TextPort.Equals("") || TextMaxKorisnika.Equals(""))
            {
                TextBlockError.Text = "Morate upisati i port i broj korisnika!";
                return;
            }
            int port;
            int maxUsers;
            try
            {
                port = Int32.Parse(TextPort);
                maxUsers = Int32.Parse(TextMaxKorisnika);
            }
            catch (FormatException)
            {
                TextBlockError.Text = "U textbox nisu upisani brojevi!";
                return;
            }
            if(port < 0 || maxUsers < 0)
            {
                TextBlockError.Text = "Upisani podaci moraju biti pozitivni!";
                return;
            }
            if(port > 65535)
            {
                TextBlockError.Text = "Port mora biti između 0 i 65535!";
                return;
            }
            if(maxUsers < 1)
            {
                TextBlockError.Text = "Morate dozvoliti rad bar jednom korisniku!";
                return;
            }

            ServerParametersRepository serverParametersRepository = new ServerParametersRepository();
            serverParametersRepository.InsertOrUpdate(new ServerParameters(port, maxUsers));
            TextBlockError.Foreground = new SolidColorBrush(Colors.Green);
            TextBlockError.Text = "Parametri servera uspješno promijenjeni!";
        }

        private void BtnAddUser_Click(object sender, RoutedEventArgs e)
        {
            TextBlockError.Foreground = new SolidColorBrush(Colors.Red);
            TextBlockError.Text = "";
            string username = TextBoxUsername.Text;
            string password = TextBoxPassword.Password;
            string email = TextBoxEmail.Text;
            if(username.Equals("") || password.Equals("") || email.Equals(""))
            {
                TextBlockError.Text = "Morate upisati sve podatke za registraciju!";
                return;
            }
            try
            {
                RegistrationUser newUser = new RegistrationUser();
                newUser.UserName = username;
                newUser.Password = password;
                newUser.Email = email;
                newUser.RoleId = 0;
                _userRepo.Insert(newUser.CreateUser());
                TextBlockError.Foreground = new SolidColorBrush(Colors.Green);
                TextBlockError.Text = "Korisnik uspješno dodan!";
            }
            catch (Exception)
            {
                TextBlockError.Text = "Upisani podaci nisu jedinstveni!";
                return;
            }
            _users = _userRepo.GetAllWinemakers(_adminId).ToList();

            ListViewWiners.ClearValue(ItemsControl.ItemsSourceProperty);
            ListViewWiners.ItemsSource = _users;
        }

        private void ListViewWiners_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            TextBlockError.Foreground = new SolidColorBrush(Colors.Red);
            TextBlockError.Text = "";
            User selected = (User) ListViewWiners.SelectedItem;

            bool? signedIn = _userRepo.GetById(selected.Id).SignedIn;
            bool loggedIn = signedIn != null && (bool)signedIn;

            if(loggedIn)
            {
                TextBlockError.Text = "Korisnik " + selected.GetUserTitle() + " je trenutno prijavljen!";
                return;
            }
            MessageBoxResult result =
                MessageBox.Show("Jeste li sigurni da želite obrisati vinara " + selected.UserName + "?",
                "Obavijest", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if(result == MessageBoxResult.Yes)
            {
                UserInGroupRepository userInGroupRepository = new UserInGroupRepository();
                GroupRepository groupRepository = new GroupRepository();
                List<int> groups = userInGroupRepository.GetUsersGroups(selected.Id);

                foreach (int userGroup in groups)
                {
                    groupRepository.Delete(groupRepository.GetById(userGroup));
                }

                _blockedUsersRepository.DeleteByUserId(selected.Id);
                _userRepo.Delete(selected);
                _users = _userRepo.GetAllWinemakers(_adminId).ToList();
                ListViewWiners.ItemsSource = _users;
                TextBlockError.Foreground = new SolidColorBrush(Colors.Green);
                TextBlockError.Text = "Korisnik " +selected.GetUserTitle() +" je uspješno obrisan!";
            }
            return;
        }

        private void BtnShutDownServer_Click(object sender, RoutedEventArgs e)
        {
            //Davor help, treba javit serveru
            TextBlockError.Text = "";
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.User = _userRepo.GetById(_adminId);
            serverRequest.ServerRequestType = ServerRequestType.ShutDownServer;

            _client.SendRequest(serverRequest);
            TextBlockError.Foreground = new SolidColorBrush(Colors.Green);
            TextBlockError.Text = "Server uspješno ugašen!";
        }

        private void BtnLogOut_Click(object sender, RoutedEventArgs e)
        {
            _loginWindow.Show();
            this.Hide();
        }

        private void ShutDown(object sender, CancelEventArgs e)
        {
            _loginWindow.Close();
            Application.Current.Shutdown();
        }
    }
}
