﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Common
{
    public static class Constants
    {
        public static int NotificationControlId => 0;
        public static int RequestControlId => 1;
        public static int GroupControlId => 2;
        public static int ConversationControId => 3;
    }
}
