﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Text;
using System.Xml.Linq;
using Marinci.MaliVinari.Business.Models;

namespace Marinci.MaliVinari.Business
{
    public static class CommonMethods
    {
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "";
        }

        public static string UploadImage(String filePath)
        {
            try
            {
                using (var w = new WebClient())
                {
                    string clientID = "09213975f2de5b8";
                    w.Headers.Add("Authorization", "Client-ID " + clientID);
                    var values = new NameValueCollection
                    {
                        {"image", Convert.ToBase64String(File.ReadAllBytes(filePath))}
                    };

                    byte[] response = w.UploadValues("https://api.imgur.com/3/image.xml", values);


                    var xmlResponse = XDocument.Load(new MemoryStream(response));

                    return xmlResponse.Descendants("link").First().FirstNode.ToString();
                }
            }
            catch
            {
                return null;
            }
        }

        public static void SendMail(string emailAddress, User senderUser, string message)
        {
            MailMessage email = new MailMessage();
            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");

            email.From = new MailAddress("marincivinari@gmail.com");
            email.To.Add(new MailAddress(emailAddress));
            email.Subject = "MaliVinari poruka";
            email.Body = "Korisnik " + senderUser.FirstName + " " + senderUser.LastName + " šalje poruku : " + message;

            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("marincivinari", "l7MOjH5puKT7F5HoqT");
            smtpServer.EnableSsl = true;

            smtpServer.Send(email);
        }

        public static string GetRequestMessage(Request request)
        {
            RequestRepository requestRepository = new RequestRepository();
            GroupRepository groupRepository = new GroupRepository();

            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.Append("Imate zahtjev za " +
                      requestRepository.GetRequestName(request,
                          request.RequestType) + " grupe " +
                      groupRepository.GetById(request.GroupID).GroupName + ".");
            sb.AppendLine();
            sb.Append("Prihvaćate li zahtjev?");

            return sb.ToString();
        }
    }
}
