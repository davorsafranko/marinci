﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Models.HelperModels;

namespace Marinci.MaliVinari.Business
{
    public class ServerRequest
    {
        public int UserControlId = -1;
        public int WindowId = -1;
        public User User;
        public Winery Winery;
        public List<Wine> Wines;
        public List<Group> Groups;
        public List<User> Winemakers;
        public List<Request> Requests;
        public List<Notification> Notifications;
        public List<Conversation> GroupConversations; 
        public GroupRequestModel GroupRequestModel;
        public ApplicationParameters ApplicationParameters;
        public Request RequestAnswer;
        public Request SendRequest;
        public Notification Notification;
        public Conversation Conversation;
        public Group Group;
        public ServerParameters ServerParameters;
        public bool BlockUserType;
        public int UserId;
        public int NotificationReadId;
        public int? GroupId;
        public int ListeningPort;
        public string IpAddress;
        public string Message;
        public string SearchText;
        public bool GroupLoggedIn;
        public ServerRequestType ServerRequestType;
    }
}
