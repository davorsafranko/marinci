﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business
{
    public enum ServerRequestType
    {
        LogIn = 0,
        Register,
        CreateGroup,
        GetWinemakers,
        ChangeApplicationParameters,
        NotificationRead,
        RequestAnswered,
        DeleteUser,
        UpdateUser,
        GetBlockedUsers,
        LogOut,
        MaxUsersLimit,
        ServerNotAvailable,
        GetWineryAndWines,
        ShutDownServer,
        BlockUser,
        Notification,
        GroupCreated,
        GetGroupUsers,
        GroupLoggedIn,
        StartConversation,
        SendMessage,
        LeaveGroup,
        GetConversationHistory,
        GetConversationMessages,
        SendDirectMessage,
        CloseGroup,
        ChangeServerParameters,
        GroupClosed,
        EndConversation
    }
}
