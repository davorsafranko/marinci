﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Common
{
    public enum GroupRequestType
    {
        CreateGroup = 0,
        Chat,
        CloseGroup,
        EndConversation
    }
}
