﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models
{
    public class BlockedUser
    {
        public int Id { get; set; }
        public int BlockingUserId { get; set; }
        public int BlockedUserId { get; set; }
    }
}
