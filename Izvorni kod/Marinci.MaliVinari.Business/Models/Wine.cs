﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models
{
    public class Wine
    {
        public int Id { get; set; }
        public int WineryId { get; set; }
        public string Name { get; set; }
        public DateTime? MadeOn { get; set; }
        public int? Quantity { get; set; }
        public string PicturePath { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
