﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models
{
    public class Request
    {
        public int Id { get; set; }
        public int GroupID { get; set; }
        public int UserId { get; set; }
        public int RequestType { get; set; }
        public bool Accepted { get; set; }
        public bool IsRead { get; set; }
        public string RequestMessage { get; set; }
        public int? ConversationId { get; set; }

        public Request()
        {
            IsRead = false;
        }
    }
}
