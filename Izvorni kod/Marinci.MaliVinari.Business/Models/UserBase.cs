﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using Marinci.MaliVinari.Business.Models.Validation;

namespace Marinci.MaliVinari.Business.Models
{
    public class UserBase : IValidatable
    {
        [Required(ErrorMessage = "Korisničko ime je obavezno.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Duljina korisničkog imena mora biti između 3 i 30 znakova.")]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Nedozvoljen format korisnčkog imena.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Email je obavezan.")]
        [StringLength(254, MinimumLength = 3, ErrorMessage = "Duljina mail-a mora biti između 3 i 254 znakova.")]
        [RegularExpression(@"^[a-z0-9][-a-z0-9.!#$%&'*+-=?^_`{|}~\/]+@([-a-z0-9]+\.)+[a-z]{2,5}$", ErrorMessage = "Neispravan e-mail.")]
        public string Email { get; set; }

        [StringLength(50, ErrorMessage = "Ime mora biti kraće od 50 znakova.")]
        public string FirstName { get; set; }

        [StringLength(50, ErrorMessage = "Prezime mora biti kraće od 50 znakova.")]
        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public int? RoleId { get; set; }



        public void Validate()
        {
            string result = VinariValidator.Validate(this);
            if (result != null)
            {
                throw new ValidationException(result);
            }
        }

        public override string ToString()
        {
            return $"{this.FirstName} {this.LastName}({this.UserName})";
        }
    }
}
