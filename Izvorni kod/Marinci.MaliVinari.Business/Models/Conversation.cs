﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models
{
    public class Conversation
    {
        public int Id { get; set; }
        public DateTime StartedOn { get; set; }
        public int GroupId { get; set; }
        public DateTime? EndedOn { get; set; }
    }
}
