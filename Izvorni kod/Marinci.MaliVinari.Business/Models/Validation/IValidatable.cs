﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models.Validation
{
    public interface IValidatable
    {
        /// <summary>
        /// Validates a business model.
        /// </summary>
        /// <returns>A string containing all the errors associated with the validation scheme, null if the model is in a valid state.</returns>
        void Validate();
    }
}
