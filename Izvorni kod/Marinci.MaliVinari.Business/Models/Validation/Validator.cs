﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace Marinci.MaliVinari.Business.Models
{
    public class VinariValidator
    {
        public static string Validate(object obj) {
            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            Validator.TryValidateObject(obj, context, results, validateAllProperties: true);

            string @return = (results.Count == 0) ? null : "";
            foreach (ValidationResult result in results) {
                @return += result.ErrorMessage + '\n';
            }

            return @return;
        }
    }
}
