﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using BCrypt.Net;
using Bcrypt = BCrypt.Net.BCrypt;

namespace Marinci.MaliVinari.Business.Models
{
    public class RegistrationUser : UserBase
    {

        [Required(ErrorMessage = "Lozinka je obavezna.")]
        [StringLength(30, MinimumLength = 8, ErrorMessage = "Duljina lozinke mora biti između 8 i 30 znakova.")]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Krivi format lozinke.")]
        public string Password { get; set; }

        public User CreateUser()
        {
            User user = new User
            {
                UserName = UserName,
                Email = Email,
                FirstName = FirstName,
                LastName = LastName,
                PhoneNumber = PhoneNumber,
                PasswordHash = Bcrypt.HashPassword(Password, 8),
                RoleId = RoleId,
                Id = -1
            };

            return user;
        }

        public override string ToString()
        {
            return string.Format("{0}\n{1}", base.ToString(), Password);
        }
    }
}
