﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models
{
    public class ApplicationParameters
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string IpAddress { get; set; }
        public int Port { get; set; }
        public string SkinColor { get; set; }
    }
}
