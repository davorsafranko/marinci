﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models
{
    public class Group
    {
        private static Business.RequestRepository _reqRepo = new RequestRepository();

        public int Id { get; set; }
        public string GroupName { get; set; }
        public bool IsGroupChat { get; set; }
        public bool IsVisible { get; set; }

        public override string ToString()
        {
            return GroupName;
        }


        
        /// <param name="type"></param>
        /// <returns>Returns true if all group requests of type(type) have been accepted.</returns>
        public bool AreRequestsSatisfied(int type)
        {
            return _reqRepo.GetRequestsForGroupOfType(Id, type).All(request => request.Accepted);
        }

        public override bool Equals(object obj)
        {
            Group objekt = obj as Group;
            return objekt != null && this.Id == objekt.Id;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode() + this.GroupName.GetHashCode() + this.IsGroupChat.GetHashCode() +
                   this.IsVisible.GetHashCode();
        }
    }
}
