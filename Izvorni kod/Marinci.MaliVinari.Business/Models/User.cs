﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Models.Validation;
using Microsoft.SqlServer.Server;
using Bcrypt = BCrypt.Net.BCrypt;

namespace Marinci.MaliVinari.Business
{
    public class User : UserBase
    {
        public int Id { get; set; }

        public string PasswordHash { get; set; }

        public string Status { get; set; }

        public string PicturePath { get; set; }

        public bool? SignedIn { get; set; }


        /// <summary>
        /// Provjerava da li pružena lozinka odgovara hashu u bazi.
        /// </summary>
        /// <param name="password"></param>
        /// <returns>true ako se pruženi password podudara sa hashom u bazi</returns>
        public bool validPassword(string password) {
            return Bcrypt.Verify(password, PasswordHash);
        }

        public override string ToString()
        {
            return $"{base.ToString()}\n{PasswordHash}";
        }

        public string GetUserTitle()
        {
            return this.FirstName + " " + this.LastName + "(" + this.UserName + ")";
        }

        protected bool Equals(User other)
        {
            if (this.Id == 0 || other.Id == 0) return false;
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((User) obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public IEnumerable<User> GetBlockedUsers()
        {
            var repo = new UserRepository();
            return repo.GetBlockedUsers(this.Id);
        }
    }
}
