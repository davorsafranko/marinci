﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models
{
    public class UserInGroup
    {
        public int UserInGroupId { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
    }
}
