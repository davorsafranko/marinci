﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Common;

namespace Marinci.MaliVinari.Business.Models.HelperModels
{
    public class GroupRequestModel
    {
        public string GroupName { get; set; }
        public bool IsGroupChat { get; set; }
        public List<int> UserIds;
        public GroupRequestType GroupRequestType;
    }
}
