﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Models
{
    public class ServerParameters
    {
        public int Port { get; set; }
        public int MaxNumberOfUsers { get; set; }

        public ServerParameters(int port, int maxNumberOfUsers)
        {
            Port = port;
            MaxNumberOfUsers = maxNumberOfUsers;
        }
    }
}
