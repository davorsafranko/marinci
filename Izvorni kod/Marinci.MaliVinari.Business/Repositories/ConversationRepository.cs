﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class ConversationRepository : AbstractBusinessRepository<Razgovor, Conversation>
    {
        private DataLayer.ConversationRepository _conversationRepo;

        public ConversationRepository()
            : base(
                new DataLayer.ConversationRepository(),
                (conversation, razgovor) => conversation.Id = razgovor.idRazgovor)
        {
            _conversationRepo = Repo as DataLayer.ConversationRepository;
        }

        protected override Razgovor GetDataModel(Conversation conversation)
        {
            return new Razgovor
            {
                idRazgovor = conversation.Id,
                Vrijeme_Pocetka = conversation.StartedOn,
                Vrijeme_Zavrsetka = conversation.EndedOn,
                idGrupa = conversation.GroupId
            };
        }

        protected override Conversation GetBusinessModel(Razgovor conversation)
        {
            return new Conversation
            {
                Id = conversation.idRazgovor,
                StartedOn = conversation.Vrijeme_Pocetka,
                EndedOn = conversation.Vrijeme_Zavrsetka,
                GroupId = conversation.idGrupa
            };
        }

        public IEnumerable<Conversation> GetGroupConversations(int groupId)
        {
            return _conversationRepo.GetGroupConversations(groupId).Select(GetBusinessModel).ToList();
        }

        public Conversation GetLastGroupConversation(int groupId)
        {
            var result = _conversationRepo.GetLastGroupConversation(groupId);
            if (result != null)
            {
                return GetBusinessModel(result);
            }
            return null;
        }
    }
}
