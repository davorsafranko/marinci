﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Common;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class RequestRepository : AbstractBusinessRepository<Zahtjev, Request>
    {
        private DataLayer.RequestRepository _reqRepo;

        public RequestRepository()
            : base(new DataLayer.RequestRepository(), (request, zahtjev) => request.Id = zahtjev.idZahtjev)
        {
            _reqRepo = Repo as DataLayer.RequestRepository;
        }

        protected override Zahtjev GetDataModel(Request model)
        {
            return new Zahtjev
            {
                idZahtjev = model.Id,
                idGrupa = model.GroupID,
                idKorisnik = model.UserId,
                Tip_Zahtjeva = model.RequestType,
                Prihvacen = model.Accepted,
                Odgovoren = model.IsRead,
                idRazgovor = model.ConversationId
            };
        }

        protected override Request GetBusinessModel(Zahtjev model)
        {
            return new Request
            {
                Id = model.idZahtjev,
                GroupID = model.idGrupa,
                UserId = model.idKorisnik,
                RequestType = model.Tip_Zahtjeva,
                Accepted = model.Prihvacen,
                IsRead = model.Odgovoren,
                ConversationId = model.idRazgovor
            };
        }


        /// <param name="groupId"></param>
        /// <param name="type"></param>
        /// <returns>Requests of specified group(groupId) of type(type).</returns>
        public IEnumerable<Request> GetRequestsForGroupOfType(int groupId, int type)
        {
            return _reqRepo.GetRequestsForGroupOfType(groupId, type).Select(GetBusinessModel).ToList();
        }

        public bool AreRequestsForGroupOfTypeResponded(int groupId, int type)
        {
            return GetRequestsForGroupOfType(groupId, type).All(r => r.IsRead);
        }

        public List<Request> GetUsersRequests(int userId)
        {
            return _reqRepo.GetUsersRequests(userId).Select(GetBusinessModel).ToList();
        }

        public void DeleteRequests(List<Request> requests)
        {
            _reqRepo.DeleteRequests(requests.Select(GetDataModel).ToList());
        }

        public string GetRequestName(Request request, int groupRequestType)
        {
            if (groupRequestType == (int) GroupRequestType.CreateGroup)
            {
                return "stvaranje";
            }
            else if (groupRequestType == (int) GroupRequestType.CloseGroup)
            {
                return "zatvaranje";
            }
            else if (groupRequestType == (int) GroupRequestType.EndConversation)
            {
                return "zatvaranje trenutnog razgovora";
            }
            return "";
        }
    }
}
