﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class GroupRepository : AbstractBusinessRepository<Grupa, Group>
    {
        private readonly DataLayer.GroupRepository _groupRepo;

        public GroupRepository() : base(new DataLayer.GroupRepository(), (group, grupa) => group.Id = grupa.idGrupa)
        {
            _groupRepo = Repo as DataLayer.GroupRepository;
        }

        protected override Grupa GetDataModel(Group group)
        {
            return new Grupa
            {
                idGrupa = group.Id,
                Naziv_Grupa = group.GroupName,
                Je_Grupni_Razgovor = group.IsGroupChat,
                Kreirana =  group.IsVisible
            };
        }

        protected override Group GetBusinessModel(Grupa group)
        {
            return new Group
            {
                Id = group.idGrupa,
                GroupName = group.Naziv_Grupa,
                IsGroupChat = group.Je_Grupni_Razgovor,
                IsVisible = group.Kreirana
            };
        }



        /// <param name="userId"></param>
        /// <returns>Groups that the user with the specified userId belongs to</returns>
        public IEnumerable<Group> GetUserGroups(int userId)
        {
            return _groupRepo.GetUserGroups(userId).Select(GetBusinessModel).ToList();
        }
    }
}
