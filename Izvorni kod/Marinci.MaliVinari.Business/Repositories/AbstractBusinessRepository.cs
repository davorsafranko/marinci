﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Business.Repositories
{
    public abstract class AbstractBusinessRepository<DLM, BM> : IRepository<BM>
    {
        protected DataLayer.IRepository<DLM> Repo { get; set; }
        private Action<BM, DLM> _idAssigner;

        protected AbstractBusinessRepository(DataLayer.IRepository<DLM> repo, Action<BM, DLM> idAssigner)
        {
            Repo = repo;
            _idAssigner = idAssigner;
        }

        public void Insert(BM entity)
        {
            var dataEntity = GetDataModel(entity);
            Repo.Insert(dataEntity);
            _idAssigner(entity, dataEntity);
        }

        public void Update(BM entity)
        {
            Repo.Update(GetDataModel(entity));
        }

        public void Delete(BM entity)
        {
            Repo.Delete(GetDataModel(entity));
        }

        public IEnumerable<BM> GetAll()
        {
            return Repo.GetAll().Select(GetBusinessModel).ToList();
        }

        public BM GetById(int id)
        {
            return GetBusinessModel(Repo.GetById(id));
        }



        protected abstract DLM GetDataModel(BM model);

        protected abstract BM GetBusinessModel(DLM model);
    }
}
