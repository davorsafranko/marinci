﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class ServerParametersRepository
    {
        private readonly DataLayer.ServerParametersRepository _serverParametersRepository;

        public ServerParametersRepository()
        {
            this._serverParametersRepository = new DataLayer.ServerParametersRepository();
        }

        public void InsertOrUpdate(ServerParameters serverParameters)
        {
            Parametri_posluzitelja parameters = GetDataLayerServerParameters(serverParameters);
            _serverParametersRepository.InsertOrUpdate(parameters);
        }

        public ServerParameters GetServerParameters()
        {
            Parametri_posluzitelja parameters = _serverParametersRepository.GetServerParameters();
            return GetBusinessServerParameters(parameters);
        }

        private Parametri_posluzitelja GetDataLayerServerParameters(ServerParameters serverParameters)
        {
            Parametri_posluzitelja dto = new Parametri_posluzitelja
            {
                Port = serverParameters.Port,
                Max_Korisnika = serverParameters.MaxNumberOfUsers
            };
            return dto;
        }

        private ServerParameters GetBusinessServerParameters(Parametri_posluzitelja serverParameters)
        {
            if (serverParameters != null)
            {
                ServerParameters dto = new ServerParameters(serverParameters.Port, serverParameters.Max_Korisnika);
                return dto;
            }
            return null;
        }
    }
}
