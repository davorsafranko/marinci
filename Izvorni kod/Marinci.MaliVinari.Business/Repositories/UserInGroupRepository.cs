﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class UserInGroupRepository : AbstractBusinessRepository<Korisnik_u_grupi, UserInGroup>
    {
        private readonly DataLayer.UserInGroupRepository _userInGroupRepository;

        public UserInGroupRepository()
            : base(new DataLayer.UserInGroupRepository(), (group, grupi) => group.UserInGroupId = grupi.idKorisnikUGrupi)
        {
            _userInGroupRepository = Repo as DataLayer.UserInGroupRepository;
        }

        protected override UserInGroup GetBusinessModel(Korisnik_u_grupi model)
        {
            return new UserInGroup
            {
                UserInGroupId = model.idKorisnikUGrupi,
                UserId = model.idKorisnik,
                GroupId = model.idGrupa
            };
        }

        protected override Korisnik_u_grupi GetDataModel(UserInGroup model)
        {
            return new Korisnik_u_grupi
            {
                idKorisnikUGrupi = model.UserInGroupId,
                idKorisnik = model.UserId,
                idGrupa = model.GroupId
            };
        }

        public List<int> GetGroupUserIds(int groupId)
        {
            return _userInGroupRepository.GetGroupUserIds(groupId);
        }

        public UserInGroup GetUserInGroup(int userId, int groupId)
        {
            return GetBusinessModel(_userInGroupRepository.GetKorisnikUGrupi(userId, groupId));
        }

        public List<int> GetUsersGroups(int userId)
        {
            return _userInGroupRepository.GetUserInGroups(userId).ToList();
        } 
    }
}
