﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class NotificationRepository : AbstractBusinessRepository<Obavijest, Notification>
    {
        private readonly DataLayer.NotificationRepository _notificationRepository;

        public NotificationRepository()
            : base(
                new DataLayer.NotificationRepository(),
                (notification, obavijest) => notification.Id = obavijest.idObavijest)
        {
            _notificationRepository = Repo as DataLayer.NotificationRepository;
        }

        protected override Obavijest GetDataModel(Notification notification)
        {
            return new Obavijest
            {
                idObavijest = notification.Id,
                idKorisnik = notification.UserId,
                Sadrzaj = notification.Message,
                Je_Procitana = notification.IsRead
            };
        }

        protected override Notification GetBusinessModel(Obavijest notification)
        {
            return new Notification
            {
                Id = notification.idObavijest,
                UserId = notification.idKorisnik,
                Message = notification.Sadrzaj,
                IsRead = notification.Je_Procitana
            };
        }

        public List<Notification> GetUsersNotifications(int userId)
        {
            return _notificationRepository.GetUsersNotifications(userId).Select(GetBusinessModel).ToList();
        }

        public void SetNotificationRead(int notificationId)
        {
            _notificationRepository.SetNotificationRead(notificationId);
        }
    }
}
