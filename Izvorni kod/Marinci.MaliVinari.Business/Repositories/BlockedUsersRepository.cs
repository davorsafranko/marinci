﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class BlockedUsersRepository : AbstractBusinessRepository<Blokirani, BlockedUser>
    {
        private readonly DataLayer.BlockedUsersRepository _blockedUsersRepository;

        public BlockedUsersRepository()
            : base(new DataLayer.BlockedUsersRepository(), (user, blokirani) => user.Id = blokirani.idKorisnik)
        {
            _blockedUsersRepository = Repo as DataLayer.BlockedUsersRepository;
        }

        protected override Blokirani GetDataModel(BlockedUser blockedUser)
        {
            return new Blokirani
            {
                idBlokada = blockedUser.Id,
                idKorisnik = blockedUser.BlockingUserId,
                idBlokiranogKorisnika = blockedUser.BlockedUserId
            };
        }

        protected override BlockedUser GetBusinessModel(Blokirani blockedUser)
        {
            return new BlockedUser
            {
                Id = blockedUser.idBlokada,
                BlockingUserId = blockedUser.idKorisnik,
                BlockedUserId = blockedUser.idBlokiranogKorisnika
            };
        }

        public BlockedUser GetBlockedUserRecord(int blockingUserId, int blockedUserId)
        {
            return GetBusinessModel(_blockedUsersRepository.GetBlockedRecord(blockingUserId, blockedUserId));
        }

        public void DeleteByUserId(int userId)
        {
            _blockedUsersRepository.DeleteByUserId(userId);
        }
    }
}
