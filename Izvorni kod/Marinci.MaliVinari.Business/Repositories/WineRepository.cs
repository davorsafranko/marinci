﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class WineRepository : AbstractBusinessRepository<Vino, Wine>
    {
        private readonly DataLayer.WineRepository _wineRepo;

        public WineRepository() : base(new DataLayer.WineRepository(), (wine, vino) => wine.Id = vino.idVino)
        {
            _wineRepo = Repo as DataLayer.WineRepository;
        }

        protected override Wine GetBusinessModel(Vino model)
        {
            return new Wine
            {
                Id = model.idVino,
                WineryId = model.idVinarija,
                Name = model.Naziv_Vina,
                PicturePath = model.Slika_Vina
            };
        }

        protected override Vino GetDataModel(Wine model)
        {
            return new Vino
            {
                idVino = model.Id,
                idVinarija = model.WineryId,
                Naziv_Vina = model.Name,
                Slika_Vina = model.PicturePath
            };
        }

        public List<Wine> GetWineryWines(int wineryId)
        {
            return _wineRepo.GetWineryWines(wineryId).Select(GetBusinessModel).ToList();
        }

        public void SyncWines(IEnumerable<Wine> wines, int wineryId)
        {
            var storedWines = GetWineryWines(wineryId);

            foreach (Wine wine in wines)
            {
                if (wine.Id != 0)
                {
                    Update(wine);
                    storedWines.Remove(storedWines.Find(_w => _w.Id == wine.Id));
                }
                else
                {
                    wine.WineryId = wineryId;
                    Insert(wine);
                }
            }

            storedWines.ForEach(w => Delete(w));
        }
    }
}
