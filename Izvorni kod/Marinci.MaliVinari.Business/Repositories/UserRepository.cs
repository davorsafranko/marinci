﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class UserRepository : AbstractBusinessRepository<Korisnik, User>
    {
        private readonly DataLayer.UserRepository _userRepo;

        public UserRepository() : base(new DataLayer.UserRepository(), (user, korisnik) => user.Id = korisnik.idKorisnik)
        {
            _userRepo = Repo as DataLayer.UserRepository;
        }

        protected override User GetBusinessModel(Korisnik model)
        {
            return new User
            {
                SignedIn = model.Prijavljen,
                FirstName = model.Ime,
                Id = model.idKorisnik,
                LastName = model.Prezime,
                PasswordHash = model.Lozinka,
                PhoneNumber = model.Telefon,
                Status = model.Osobni_Status,
                UserName = model.Korisnicko_Ime,
                Email = model.Email,
                PicturePath = model.Profilna_Slika,
                RoleId = model.idUloga
            };
        }

        protected override Korisnik GetDataModel(User model)
        {
            return new Korisnik
            {
                Email = model.Email,
                Ime = model.FirstName,
                Prezime = model.LastName,
                Korisnicko_Ime = model.UserName,
                Lozinka = model.PasswordHash,
                Osobni_Status = model.Status,
                Prijavljen = model.SignedIn,
                Profilna_Slika = model.PicturePath,
                Telefon = model.PhoneNumber,
                idKorisnik = model.Id,
                idUloga = model.RoleId
            };
        }

        public User GetUserByUserName(string username)
        {
            var user = _userRepo.GetUserByUserName(username);
            if (user == null) return null;
            return GetBusinessModel(user);
        }

        /// <param name="groupId"></param>
        /// <returns>Members belonging to group with specified groupId.</returns>
        public IEnumerable<User> GetGroupMembers(int groupId)
        {
            return _userRepo.GetGroupMembers(groupId).Select(GetBusinessModel).ToList();
        }

        public User GetUserByEmail(string email)
        {
            var user = _userRepo.GetByEmail(email);
            if (user == null) return null;
            return GetBusinessModel(user);
        }

        public List<User> GetAllWinemakers(int skipUserId)
        {
            List<User> winemakers = new List<User>();

            List<Korisnik> receivedWinemakers = _userRepo.GetAllWinemakers(skipUserId);

            foreach (Korisnik winemaker in receivedWinemakers)
            {
                winemakers.Add(GetBusinessModel(winemaker));
            }

            return winemakers;
        }

        /// <summary>
        /// Finds all users whose FirstName, LastName contain the specified search term(term), also finds all wines whose name contain the search term(term) and finds their owner who is then returned as part of the resulting return value.
        /// </summary>
        /// <param name="term"></param>
        /// <returns>Matching users.</returns>
        public IEnumerable<User> Search(string term)
        {
            return _userRepo.Search(term).Select(GetBusinessModel).ToList();
        }

        /// <param name="userId"></param>
        /// <returns>Returns all users blocked by user(user_id).</returns>
        public IEnumerable<User> GetBlockedUsers(int userId)
        {
            return _userRepo.GetBlockedUsers(userId).Select(GetBusinessModel).ToList();
        }

        public void SetUserLoggedInStatus(int userId, bool loggedIn)
        {
            _userRepo.SetUserLoggedInStatus(userId, loggedIn);
        }

        public bool CheckAdminExistence()
        {
            return _userRepo.CheckAdminExistence();
        }

        public bool CanFormGroup(IEnumerable<User> users)
        {
            return users.All(u => !u.GetBlockedUsers().Any(users.Contains));
        }
    }
}
