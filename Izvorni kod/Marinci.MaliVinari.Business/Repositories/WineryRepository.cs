﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class WineryRepository : AbstractBusinessRepository<Vinarija, Winery>
    {
        private readonly DataLayer.WineryRepository _wineryRepo;

        public WineryRepository() : base(new DataLayer.WineryRepository(), (winery, vinarija) => winery.Id = vinarija.idVinarija)
        {
            _wineryRepo = Repo as DataLayer.WineryRepository;
        }

        protected override Winery GetBusinessModel(Vinarija model)
        {
            return new Winery
            {
                Id = model.idVinarija,
                UserId = model.idKorisnik,
                Name = model.Naziv_Vinarije,
                Address = model.Adresa_Vinarije
            };
        }

        protected override Vinarija GetDataModel(Winery model)
        {
            return new Vinarija
            {
                idVinarija = model.Id,
                idKorisnik = model.UserId,
                Naziv_Vinarije = model.Name,
                Adresa_Vinarije = model.Address
            };
        }



        public Winery GetUserWinery(int userId)
        {
            var winery = _wineryRepo.GetUserWinery(userId);
            if (winery == null) { return null; }
            return GetBusinessModel(winery);
    }
    }
}
