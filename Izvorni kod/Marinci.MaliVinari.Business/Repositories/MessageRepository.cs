﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Repositories;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class MessageRepository : AbstractBusinessRepository<Poruka, Message>
    {
        private DataLayer.MessageRepository _messageRepo;

        public MessageRepository()
            : base(new DataLayer.MessageRepository(), (message, poruka) => message.Id = poruka.idPoruka)
        {
            _messageRepo = Repo as DataLayer.MessageRepository;
        }

        protected override Poruka GetDataModel(Message message)
        {
            return new Poruka
            {
                idPoruka = message.Id,
                idKorisnik = message.UserId,
                idRazgovor = message.ConversationId,
                Vrijeme = message.SentOn,
                Sadrzaj = message.Content
            };
        }

        protected override Message GetBusinessModel(Poruka message)
        {
            return new Message
            {
                Id = message.idPoruka,
                ConversationId = message.idRazgovor,
                UserId = message.idKorisnik,
                SentOn = message.Vrijeme,
                Content = message.Sadrzaj
            };
        }

        /// <param name="convoId"></param>
        /// <returns>Returns messages belonging to specified conversation(convoId).</returns>
        public IEnumerable<Message> GetConversationMessages(int convoId)
        {
            return _messageRepo.GetConversationMessages(convoId).Select(GetBusinessModel).ToList();
        }
    }
}
