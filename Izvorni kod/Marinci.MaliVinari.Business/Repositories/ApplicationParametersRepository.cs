﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.DataLayer;

namespace Marinci.MaliVinari.Business
{
    public class ApplicationParametersRepository : IRepository<ApplicationParameters>
    {
        private readonly DataLayer.ApplicationParametersRepository _applicationParametersRepository;

        public ApplicationParametersRepository()
        {
            this._applicationParametersRepository = new DataLayer.ApplicationParametersRepository();
        }

        public void InsertOrUpdate(ApplicationParameters entity)
        {
            this._applicationParametersRepository.InsertOrUpdate(GetDataLayerApplicationParameters(entity));
        }

        public void Insert(ApplicationParameters entity)
        {
            throw new NotImplementedException();
        }

        public void Update(ApplicationParameters entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(ApplicationParameters entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ApplicationParameters> GetAll()
        {
            throw new NotImplementedException();
        }

        public ApplicationParameters GetById(int id)
        {
            throw new NotImplementedException();
        }

        private DataLayer.Parametri_aplikacije GetDataLayerApplicationParameters(ApplicationParameters applicationParameters)
        {
            Parametri_aplikacije dto = new Parametri_aplikacije
            {
                Boja_Skin = applicationParameters.SkinColor,
                IP_Adresa = applicationParameters.IpAddress.ToString(),
                Port = applicationParameters.Port,
                idParametar = applicationParameters.Id,
                idKorisnik = applicationParameters.UserId
            };
            return dto;
        }

        private ApplicationParameters GetBusinessApplicationParameters(Parametri_aplikacije applicationParameters)
        {
            ApplicationParameters dto = new ApplicationParameters
            {
                Id = applicationParameters.idParametar,
                IpAddress = applicationParameters.IP_Adresa,
                Port = applicationParameters.Port,
                SkinColor = applicationParameters.Boja_Skin,
                UserId = applicationParameters.idKorisnik
            };
            return dto;
        }

        public ApplicationParameters GetParametersByUserId(int userId)
        {
            var result = _applicationParametersRepository.GetParametersByUserId(userId);
            if (result != null)
            {
                return GetBusinessApplicationParameters(result);
            }
            return null;
        }
    }
}
