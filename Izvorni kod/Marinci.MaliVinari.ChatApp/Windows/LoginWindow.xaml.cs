﻿using System;
using System.ComponentModel;
using System.Windows;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;

namespace Marinci.MaliVinari.ChatApp.Windows
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private Client _client;
        private User _user = null;
        private string _password;

        public LoginWindow()
        {
            InitializeComponent();

            Closing += ShutDown;

            //hardkodiramo IP adresu servera, svako kod sebe mora kod testiranja ovo promijeniti
            _client = new Client("192.168.217.1", 50001);
            _client.StartClient();
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            _client.Shutdown();
            Application.Current.Shutdown();
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            Window regWindow = new RegisterWindow(_client, this);
            regWindow.Visibility = Visibility.Visible;
            this.Hide();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            TextBlockWarning.Text = "";

            string username = TextBoxUsername.Text;
            string password = PasswordBox.Password;

            if (username.Equals("") || password.Equals(""))
            {
                TextBlockWarning.Text = "Morate unijeti i korisničko ime i lozinku!";
                return;
            }

            ServerRequest serverRequest = new ServerRequest();
            serverRequest.ListeningPort = _client.GetClientEndPoint().Port;
            serverRequest.IpAddress = _client.GetClientEndPoint().Address.ToString();
            serverRequest.ServerRequestType = ServerRequestType.LogIn;

            _user = new User();
            _user.UserName = username;
            _password = password;
            _user.Id = -1;

            serverRequest.User = _user;

            _client.SendRequestWindows(this, serverRequest);
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            //mora preko dispatchera jer updateamo gui iz druge dretve
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.MaxUsersLimit)
                {
                    MessageBox.Show("Poslužitelj je pod maksimalnim opterećenjem!", "Pogreška",
                          MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (serverRequest.ServerRequestType != ServerRequestType.ServerNotAvailable)
                {
                    _user = serverRequest.User;

                    if (_user != null)
                    {
                        if (!_user.validPassword(_password))
                        {
                            TextBlockWarning.Text = "Lozinka nije točna!";
                            return;
                        }

                        foreach (Request request in serverRequest.Requests)
                        {
                            request.RequestMessage = CommonMethods.GetRequestMessage(request);
                        }

                        UserData userData = new UserData(serverRequest.User, serverRequest.Winery, serverRequest.Wines,
                            serverRequest.Groups, serverRequest.Notifications, serverRequest.Requests,
                            serverRequest.ApplicationParameters);

                        Window Interface = new UserInterfaceWindow(userData, _client, this);
                        Interface.Show();

                        this.Hide();
                    }
                    else
                    {
                        TextBlockWarning.Text = "Ne postoji korisnik sa zadanim korisničkim imenom!";
                    }
                }
                else
                {
                    MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                         MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }));
        }

        private void ShutDown(object sender, CancelEventArgs e)
        {
            //Treba još javiti serveru da se user odjavljuje iz sustava
            if (_user != null)
            {
                ServerRequest serverRequest = new ServerRequest();
                serverRequest.User = _user;
                serverRequest.ServerRequestType = ServerRequestType.LogOut;

                _client.SendRequestWindows(this, serverRequest);
            }

            _client.Shutdown();
            Application.Current.Shutdown();
        }
    }
}
