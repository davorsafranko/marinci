﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.ChatApp.Models;

namespace Marinci.MaliVinari.ChatApp.Windows
{
    /// <summary>
    /// Interaction logic for DirectMessageWindow.xaml
    /// </summary>
    public partial class DirectMessageWindow : Window
    {
        private UserControl _profileViewControl;
        private User _user;
        private UserData _userData;
        private Client _client;
        public DirectMessageWindow(UserControl profileViewControl, UserData userData, User user, Client client)
        {
            InitializeComponent();
            _profileViewControl = profileViewControl;
            _user = user;
            _userData = userData;
            _client = client;

            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e)
        {
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.User = _userData.User;
            serverRequest.UserId = _user.Id;
            serverRequest.Message = this.TextBoxMessage.Text;
            serverRequest.ServerRequestType = ServerRequestType.SendDirectMessage;

            _client.SendRequestUserControl(_profileViewControl, serverRequest);
            this.Close();
        }

        private void BtnReject_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
