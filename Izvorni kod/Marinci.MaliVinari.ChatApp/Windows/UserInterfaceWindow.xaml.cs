﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Controls;
using Marinci.MaliVinari.ChatApp.Models;
using Message = System.Windows.Forms.Message;

namespace Marinci.MaliVinari.ChatApp.Windows
{
    /// <summary>
    /// Interaction logic for UserInterfaceWindow.xaml
    /// </summary>
    public partial class UserInterfaceWindow : Window
    {
        private UserData _userData;
        private Client _client;
        private Window loginWindow;
        private List<Request> _requests; 
        private NotificationControl _notificationControl;
        private RequestControl _requestControl;
        private GroupListControl _groupListControl;
        //private GroupViewControl _groupViewControl;

        public UserInterfaceWindow(UserData userData, Client client, Window loginWindow)
        {
            this.loginWindow = loginWindow;
            InitializeComponent();
            Closing += ShutDown;
            _userData = userData;
            _client = client;

            _notificationControl = new NotificationControl(_userData, _client);
            _requestControl = new RequestControl(_userData, _client);
            _groupListControl = new GroupListControl(_userData, PanelControl, _client, this);

            _client.AddUserControl(_notificationControl);
            _client.AddUserControl(_requestControl);
            //NE ZNAM JEL OVO TREBA TU STAVIT ALI AK NE TREBA ONDA SAMO MAKNI
            _client.AddUserControl(_groupListControl);

            GroupPanel.Children.Add(_groupListControl);
            NotificationPanel.Children.Add(_notificationControl);
            RequestPanel.Children.Add(_requestControl);

            PanelControl.Children.Add(new ProfileEditControl(_client, _userData));

            this.Title += " (" + _userData.User.UserName + ")";

            if (_userData.ApplicationParameters != null)
            {
                switch (_userData.ApplicationParameters.SkinColor)
                {
                    case "RedColor":
                        this.Background = Brushes.LightCoral;
                        break;
                    case "BlueColor":
                        this.Background = Brushes.LightBlue;
                        break;
                    case "GreenColor":
                        this.Background = Brushes.DarkSeaGreen;
                        break;
                    case "BeigeColor":
                        this.Background = Brushes.Beige;
                        break;
                    default:
                        this.Background = Brushes.White;
                        break;
                }
            }
        }

        private void BtnCreateGroup_OnClick(object sender, RoutedEventArgs e)
        {
            PanelControl.Children.Clear();
            PanelControl.Children.Add(new CreateGroupControl(_client, _userData.GetUser()));
        }

        private void BtnSettings_OnClick(object sender, RoutedEventArgs e)
        {
            PanelControl.Children.Clear();
            PanelControl.Children.Add(new SettingsControl(_client, _userData.GetUser(), this));
        }

        private void BtnMyProfile_OnClick(object sender, RoutedEventArgs e)
        {
            PanelControl.Children.Clear();
            PanelControl.Children.Add(new ProfileEditControl(_client, _userData));
        }

        private void BtnSearch_OnClick(object sender, RoutedEventArgs e)
        {
            if (TextBoxSearch.Text.Length >= 3)
            {
                PanelControl.Children.Clear();
                PanelControl.Children.Add(new SearchUserControl(_userData, _client, this, TextBoxSearch.Text));
            }
            else
            {
                MessageBox.Show("Morate unijeti barem 3 znaka za pretraživanje!", "Upozorenje", MessageBoxButton.OK,MessageBoxImage.Warning);
            }
        }

        private void ShutDown(object sender, CancelEventArgs e)
        {
            //Treba još javiti serveru da se user odjavljuje iz sustava
            _client.Shutdown();
            Application.Current.Shutdown();
        }

        private void BtnLogout_OnClick(object sender, RoutedEventArgs e)
        {
            //Treba još javiti serveru da se user odjavljuje iz sustava
            if (_userData.GetUser() != null)
            {
                ServerRequest serverRequest = new ServerRequest();
                serverRequest.User = _userData.GetUser();
                serverRequest.ServerRequestType = ServerRequestType.LogOut;
                serverRequest.Conversation = _userData.CurrentConversation;

                _client.SendRequestWindows(this, serverRequest);
            }


            loginWindow.Show();
            this.Hide();
        }

        private void BtnBlockedUsers_OnClick(object sender, RoutedEventArgs e)
        {
            PanelControl.Children.Clear();
            PanelControl.Children.Add(new SearchUserControl(_userData, _client, this,"",true));
        }

        private void BtnAllUsers_OnClick(object sender, RoutedEventArgs e)
        {
            PanelControl.Children.Clear();
            PanelControl.Children.Add(new SearchUserControl(_userData, _client, this));
        }

        public StackPanel GetPanelControl()
        {
            return PanelControl;
        }

        public void DisableElements()
        {
            GroupPanel.IsEnabled = false;
            BtnAllUsers.IsEnabled = false;
            BtnBlockedUsers.IsEnabled = false;
            BtnCreateGroup.IsEnabled = false;
            BtnMyProfile.IsEnabled = false;
            BtnSearch.IsEnabled = false;
            BtnSettings.IsEnabled = false;
        }

        public void EnableElements()
        {
            GroupPanel.IsEnabled = true;
            BtnAllUsers.IsEnabled = true;
            BtnBlockedUsers.IsEnabled = true;
            BtnCreateGroup.IsEnabled = true;
            BtnMyProfile.IsEnabled = true;
            BtnSearch.IsEnabled = true;
            BtnSettings.IsEnabled = true;
        }
    }
}
