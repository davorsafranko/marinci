﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Controls;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace Marinci.MaliVinari.ChatApp.Windows
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        private Client _client;
        private Window _loginWindow;
        private User _user = null;
        private WineEditingControl _wineEditingControl;

        public RegisterWindow(Client client, Window loginWindow)
        {
            _loginWindow = loginWindow;
            InitializeComponent();
            Closing += ShutDown;
            _client = client;
            _wineEditingControl = new WineEditingControl();
            WineEditingPanel.Children.Add(_wineEditingControl);
            TextBlockWarning.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            _loginWindow.Show();
            this.Hide();
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            TextBlockWarning.Text = "";
            TextBlockWarning.Foreground = new SolidColorBrush(Colors.Red);
            try
            {
                RegistrationUser user = new RegistrationUser();
                user.UserName = TxtBoxUsername.Text;
                user.Email = TextBoxEmail.Text;
                user.Password = PasswordBox.Password;

                user.FirstName = TxtBoxName.Text;
                user.LastName = TextBoxLastName.Text;
                user.PhoneNumber = TextBoxPhoneNumber.Text;
                user.RoleId = 0;

                user.Validate();

                ServerRequest serverRequest = new ServerRequest();
                serverRequest.User = user.CreateUser();

                _user = serverRequest.User;

                Winery winery = new Winery();

                if (!String.IsNullOrEmpty(TextBoxWineryName.Text))
                {
                    winery.Name = TextBoxWineryName.Text;
                    winery.Address = TextBoxWineryAddress.Text;
                }

                serverRequest.Winery = winery;

                if (_wineEditingControl.GetWineList().Count > 0)
                {
                    List<Wine> wines = _wineEditingControl.GetWineList();
                    foreach (Wine wine in wines)
                    {
                        if (!String.IsNullOrEmpty(wine.PicturePath))
                        {
                            wine.PicturePath = CommonMethods.UploadImage(wine.PicturePath);
                        }
                    }
                    serverRequest.Wines = wines;
                }
                
                serverRequest.IpAddress = _client.GetClientEndPoint().Address.ToString();
                serverRequest.ListeningPort = _client.GetClientEndPoint().Port;
                serverRequest.ServerRequestType = ServerRequestType.Register;

                _client.SendRequestWindows(this, serverRequest);
            }
            catch (ValidationException exception)
            {
                TextBlockWarning.Text = exception.Message;
            }
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.MaxUsersLimit)
                {
                    MessageBox.Show("Poslužitelj je pod maksimalnim opterećenjem!", "Pogreška",
                          MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (serverRequest.ServerRequestType != ServerRequestType.ServerNotAvailable)
                {
                    if (!String.IsNullOrEmpty(serverRequest.Message))
                    {
                        TextBlockWarning.Text = serverRequest.Message;
                    }
                    else
                    {
                        TextBlockWarning.Text = "Registracija je uspješna!";
                        TextBlockWarning.Foreground = new SolidColorBrush(Colors.Green);
                    }
                }
                else
                {
                    MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }));
        }

        private void ShutDown(object sender, CancelEventArgs e)
        {
            //Treba još javiti serveru da se user odjavljuje iz sustava
            if (_user != null)
            {
                ServerRequest serverRequest = new ServerRequest();
                serverRequest.User = _user;
                serverRequest.ServerRequestType = ServerRequestType.LogOut;

                _client.SendRequestWindows(this, serverRequest);
            }

            _client.Shutdown();
            Application.Current.Shutdown();
        }
    }
}
