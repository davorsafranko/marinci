﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;
using Marinci.MaliVinari.ChatApp.Windows;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for GroupViewControl.xaml
    /// </summary>
    public partial class GroupViewControl : UserControl
    {
        private UserData _userData;
        private Client _client;
        private Group _group;
        private Panel _controlPanel;
        private User _selectedUser;
        private Conversation _conversation;
        private List<Conversation> _conversationHistory;
        private UserInterfaceWindow _userInterface;

        public GroupViewControl(UserData userData, Client client, Group group, Panel controlPanel, UserInterfaceWindow userInterface)
        {
            InitializeComponent();

            _controlPanel = controlPanel;
            _userData = userData;
            _client = client;
            _group = group;
            _conversation = new Conversation();
            _userInterface = userInterface;

            if(!String.IsNullOrEmpty(_group.GroupName)) { 
                TextBlockGroupName.Text = _group.GroupName;
            }

            ServerRequest serverRequest = new ServerRequest();
            serverRequest.ServerRequestType = ServerRequestType.GetGroupUsers;
            serverRequest.User = _userData.User;
            serverRequest.Group = group;

            _client.SendRequestUserControl(this, serverRequest);

            serverRequest = new ServerRequest();
            serverRequest.ServerRequestType = ServerRequestType.GetConversationHistory;
            serverRequest.User = _userData.User;
            serverRequest.Group = group;

            _client.SendRequestUserControl(this, serverRequest);
        }

        private void ListViewConversations_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListViewConversations.SelectedItem != null) { 
                _controlPanel.Children.Clear();
                _controlPanel.Children.Add(new ConversationHistroyControl((Conversation)ListViewConversations.SelectedItem, _userData, _client));
            }
        }

        private void ListViewMembers_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListViewMembers.SelectedItem != null)
            {
                ServerRequest serverRequest = new ServerRequest();

                serverRequest.User = _userData.User;
                serverRequest.ServerRequestType = ServerRequestType.GetWineryAndWines;
                serverRequest.UserId = ((User) ListViewMembers.SelectedItem).Id;

                _selectedUser = (User) ListViewMembers.SelectedItem;

                if (_selectedUser.Id != _userData.User.Id)
                {
                    _client.SendRequestUserControl(this, serverRequest);
                }
                else
                {
                    ProfileEditControl profileEditControl = new ProfileEditControl(_client, _userData);

                    _controlPanel.Children.Clear();
                    _controlPanel.Children.Add(profileEditControl);
                }
            }
        }

          public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.ServerNotAvailable)
                {
                    MessageBoxResult result = MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    if (serverRequest.ServerRequestType == ServerRequestType.GetWineryAndWines)
                    {
                        ProfileViewControl profileViewControl = new ProfileViewControl(_userData,_selectedUser, serverRequest.Winery, serverRequest.Wines, _client);

                        _controlPanel.Children.Clear();
                        _controlPanel.Children.Add(profileViewControl);
                    }
                    else if (serverRequest.ServerRequestType == ServerRequestType.GetGroupUsers)
                    {
                        ListViewMembers.ClearValue(ItemsControl.ItemsSourceProperty);
                        ListViewMembers.ItemsSource = serverRequest.Winemakers;
                    }
                    else if (serverRequest.ServerRequestType == ServerRequestType.GroupLoggedIn)
                    {
                        if (serverRequest.GroupLoggedIn == false)
                        {
                            MessageBox.Show("Netko od članova grupe nije prijavljen i ne može početi komunikacija!",
                                "Obavijest", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        else
                        {
                            ServerRequest startConversationRequest = new ServerRequest();
                            startConversationRequest.ServerRequestType = ServerRequestType.StartConversation;
                            startConversationRequest.User = _userData.User;
                            startConversationRequest.Group = _group;

                            _client.SendRequestUserControl(this, startConversationRequest);
                        }
                    }
                    else if (serverRequest.ServerRequestType == ServerRequestType.StartConversation)
                    {
                        _conversation = serverRequest.Conversation;

                        _controlPanel.Children.Clear();

                        ConversationControl cControl = new ConversationControl(_userData, _client, serverRequest.Group,
                            serverRequest.Message, _conversation, _userInterface);

                        _client.AddUserControl(cControl);

                        _controlPanel.Children.Add(cControl);
                    }
                    else if (serverRequest.ServerRequestType == ServerRequestType.GetConversationHistory)
                    {
                        _conversationHistory = serverRequest.GroupConversations;
                        ListViewConversations.ClearValue(ItemsControl.ItemsSourceProperty);
                        ListViewConversations.ItemsSource = _conversationHistory;
                    }
                    else if (serverRequest.ServerRequestType == ServerRequestType.CloseGroup)
                    {
                        if (String.IsNullOrEmpty(serverRequest.Message))
                        {
                            TextBlockWarning.Text = "Poslan je zahtjev za uklanjanje grupe!";
                            TextBlockWarning.Foreground = Brushes.Green;

                        }
                        else
                        {
                            TextBlockWarning.Text = "Došlo je do pogreške pri slanju zahtjeva!";
                            TextBlockWarning.Foreground = Brushes.Red;
                        }
                    }
                }
            }));
        }

        private void BtnStartConversation_Click(object sender, RoutedEventArgs e)
        {
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.User = _userData.User;
            serverRequest.Group = _group;
            serverRequest.ServerRequestType = ServerRequestType.GroupLoggedIn;

            _client.SendRequestUserControl(this, serverRequest);
        }

        private void BtnLeaveGroup_Click(object sender, RoutedEventArgs e)
        {
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.User = _userData.User;
            serverRequest.Group = _group;
            serverRequest.ServerRequestType = ServerRequestType.LeaveGroup;

            _client.SendRequestUserControl(this, serverRequest);

            _userData.Groups.Remove(_group);

            _client.UpdateGroupList(_userData);

            _controlPanel.Children.Clear();
            _controlPanel.Children.Add(new ProfileEditControl(_client, _userData));
        }

        private void BtnDisbandGroup_Click(object sender, RoutedEventArgs e)
        {
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.User = _userData.User;
            serverRequest.Group = _group;
            serverRequest.ServerRequestType = ServerRequestType.CloseGroup;

            _client.SendRequestUserControl(this, serverRequest);
        }
    }
}
