﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;
using UserControl = System.Windows.Controls.UserControl;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for WineEditingControl.xaml
    /// </summary>
    public partial class WineEditingControl : UserControl
    {
        private OpenFileDialog _fileDialog;
        private List<Wine> _wineList;
        private UserData _userData;

        public WineEditingControl(UserData userData)
        {
            InitializeComponent();
            if (userData.Wines == null)
            {
                userData.Wines = new List<Wine>();
            }

            _wineList = userData.Wines;
            _userData = userData;
            ListViewWine.ItemsSource = _wineList;
        }

            public WineEditingControl()
            {
                InitializeComponent();

                _wineList = new List<Wine>();
                ListViewWine.ItemsSource = _wineList;
            }

        public List<Wine> GetWineList()
        {
            return this._wineList;
        }

        private void BtnAddWine_Click(object sender, RoutedEventArgs e)
        {
            if (TextBoxWineName.Text != "")
            {
                Wine element = new Wine(); ;
                if (_fileDialog != null)
                {
                    element.Name = TextBoxWineName.Text;
                    element.PicturePath = _fileDialog.FileName ?? "";
                }
                else
                {
                    element.Name = TextBoxWineName.Text;
                }
                _wineList.Add(element);
               ListViewWine.ClearValue(ItemsControl.ItemsSourceProperty);
               ListViewWine.ItemsSource = _wineList;
            }
            _fileDialog = null;
            TextBoxWineName.Text = "";
        }

        private void BtnRemoveVine_OnClick(object sender, RoutedEventArgs e)
        {
            if (ListViewWine != null && !ListViewWine.Items.IsEmpty)
            {
                _wineList.Remove((Wine) ListViewWine.SelectedItem);
                ListViewWine.ClearValue(ItemsControl.ItemsSourceProperty);
                ListViewWine.ItemsSource = _wineList;
            }
        }

        private void BtnAddWinePicture_OnClick(object sender, RoutedEventArgs e)
        {
            if (ListViewWine.SelectedItem != null)
            { 
                _fileDialog = new OpenFileDialog();
                _fileDialog.ShowDialog();
                if (String.IsNullOrEmpty(((Wine)ListViewWine.SelectedItem).PicturePath)) {
                    Wine selectedWine =_wineList.Find(wine => wine.Name == ((Wine) ListViewWine.SelectedItem).Name);
                    selectedWine.PicturePath = _fileDialog.FileName;

                    ListViewWine.ClearValue(ItemsControl.ItemsSourceProperty);
                    ListViewWine.ItemsSource = _wineList;
                    
                    _fileDialog = null;
                }
            }

        }
    }
}
