﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.ChatApp.Models;
using Marinci.MaliVinari.ChatApp.Windows;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for SearchUserControl.xaml
    /// </summary>
    public partial class SearchUserControl : UserControl
    {
        UserData _userData;
        private Client _client;
        private List<User> _allUsers;
        private UserInterfaceWindow _window;
        private User _selectedUser;

        public SearchUserControl(UserData userData, Client client, UserInterfaceWindow window, string text = "", bool getBlockedUsers = false)
        {
            _window = window;
            SendGetWinemakersRequest(userData, client, text, getBlockedUsers);
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.ServerNotAvailable)
                {
                    MessageBoxResult result = MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    _allUsers = serverRequest.Winemakers;
                    ListViewSearch.ItemsSource = _allUsers;

                    if (serverRequest.ServerRequestType == ServerRequestType.GetWineryAndWines)
                    {
                        ProfileViewControl profileViewControl = new ProfileViewControl(_userData,_selectedUser, serverRequest.Winery, serverRequest.Wines, _client);

                        _window.GetPanelControl().Children.Clear();
                        _window.GetPanelControl().Children.Add(profileViewControl);
                    }
                }
            }));
        }

        private void SendGetWinemakersRequest(UserData userData, Client client, string text, bool getBlockedUsers)
        {
            _client = client;
            _userData = userData;
            InitializeComponent();

            ServerRequest serverRequest = new ServerRequest();

            if (!getBlockedUsers)
            {
                serverRequest.User = userData.User;
                serverRequest.ServerRequestType = ServerRequestType.GetWinemakers;
                serverRequest.SearchText = text;
            }
            else
            {
                serverRequest.User = userData.User;
                serverRequest.ServerRequestType = ServerRequestType.GetBlockedUsers;
            }

            _client.SendRequestUserControl(this, serverRequest);
        }

        private void ListViewSearch_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ServerRequest serverRequest = new ServerRequest();

            serverRequest.User = _userData.User;
            serverRequest.ServerRequestType = ServerRequestType.GetWineryAndWines;
            serverRequest.UserId = ((User) ListViewSearch.SelectedItem).Id;

            _selectedUser = (User) ListViewSearch.SelectedItem;

            _client.SendRequestUserControl(this, serverRequest);
        }
    }
}
