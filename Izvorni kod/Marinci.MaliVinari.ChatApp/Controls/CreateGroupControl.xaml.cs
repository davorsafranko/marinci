﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Common;
using Marinci.MaliVinari.Business.Models.HelperModels;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for CreateGroupControl.xaml
    /// </summary>
    public partial class CreateGroupControl : UserControl
    {
        private Client _client;
        private List<User> _allUsers;
        private User _currentUser;
        private List<User> _membersList;

        public CreateGroupControl(Client client, User user)
        {
            InitializeComponent();
            _client = client;
            _currentUser = user;
            _membersList = new List<User>();

            ServerRequest serverRequest = new ServerRequest();
            serverRequest.User = _currentUser;
            serverRequest.ServerRequestType = ServerRequestType.GetWinemakers;

            _client.SendRequestUserControl(this, serverRequest);
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.ServerNotAvailable)
                {
                    MessageBoxResult result = MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    if (serverRequest.ServerRequestType == ServerRequestType.GetWinemakers)
                    {
                        _allUsers = serverRequest.Winemakers;
                        ListViewUsers.ItemsSource = _allUsers;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(serverRequest.Message))
                        {
                            TextBlockWarning.Text = serverRequest.Message;
                            TextBlockWarning.Foreground = Brushes.Red;
                        }
                        else
                        {
                            TextBlockWarning.Text = "Poslani zahtjevi za stvaranje grupe!";
                            TextBlockWarning.Foreground = Brushes.Green;
                        }
                    }
                }
            }));
        }

        private void ListViewUsers_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (_membersList.Count < 4 && ListViewUsers.SelectedItem != null)
            {
                _membersList.Add((User) ListViewUsers.SelectedItem);
                _allUsers.Remove((User) ListViewUsers.SelectedItem);

                ListViewUsers.ClearValue(ItemsControl.ItemsSourceProperty);
                ListViewUsers.ItemsSource = _allUsers;


                ListViewMembers.ClearValue(ItemsControl.ItemsSourceProperty);
                ListViewMembers.ItemsSource = _membersList;
            }
        }


        private void ListViewMembers_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListViewMembers.SelectedItem != null) { 
                _membersList.Remove((User)ListViewMembers.SelectedItem);
                _allUsers.Add((User)ListViewMembers.SelectedItem); 


                ListViewUsers.ClearValue(ItemsControl.ItemsSourceProperty);
                ListViewUsers.ItemsSource = _allUsers;


                ListViewMembers.ClearValue(ItemsControl.ItemsSourceProperty);
                ListViewMembers.ItemsSource = _membersList;
            }
        }

        private void BtnCreateGroup_OnClick(object sender, RoutedEventArgs e)
        {
            if (_membersList.Count == 0)
            {
                TextBlockWarning.Text = "Grupa mora imati više od jednog člana da se može stvoriti!";
                TextBlockWarning.Foreground = Brushes.Red;

            }
            else if (TextBoxGroupName.Text == "")
            {
                TextBlockWarning.Text = "Grupa mora imati ime";
                TextBlockWarning.Foreground = Brushes.Red;
            }
            else
            {
                GroupRequestModel groupRequestModel = new GroupRequestModel();
                groupRequestModel.GroupName = TextBoxGroupName.Text;
                groupRequestModel.GroupRequestType = (int) GroupRequestType.CreateGroup;
                groupRequestModel.IsGroupChat = true;
                groupRequestModel.UserIds = _membersList.Select(u => u.Id).ToList();

                ServerRequest serverRequest = new ServerRequest();
                serverRequest.User = _currentUser;
                serverRequest.ServerRequestType = ServerRequestType.CreateGroup;
                serverRequest.GroupRequestModel = groupRequestModel;

                _client.SendRequestUserControl(this, serverRequest);
            }
        }
    }
}
