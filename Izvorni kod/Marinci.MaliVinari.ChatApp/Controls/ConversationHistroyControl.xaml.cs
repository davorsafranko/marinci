﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for ConversationHistroyControl.xaml
    /// </summary>
    public partial class ConversationHistroyControl : UserControl
    {
        private string _messages;
        private UserData _userData;
        private Client _client;

        public ConversationHistroyControl(Conversation conversation, UserData userData, Client client)
        {
            _userData = userData;
            _client = client;
            InitializeComponent();

            ServerRequest serverRequest = new ServerRequest();
            serverRequest.ServerRequestType = ServerRequestType.GetConversationMessages;
            serverRequest.User = _userData.User;
            serverRequest.Conversation = conversation;

            _client.SendRequestUserControl(this, serverRequest);
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.ServerNotAvailable)
                {
                    MessageBoxResult result = MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                _messages = serverRequest.Message;
                foreach (var message in _messages)
                {
                    TextBlockMessages.Text += message;
                }
            }));
        }
    }
}
