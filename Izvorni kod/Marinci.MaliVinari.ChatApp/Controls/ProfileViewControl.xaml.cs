﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Common;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.Business.Models.HelperModels;
using Marinci.MaliVinari.ChatApp.Models;
using Marinci.MaliVinari.ChatApp.Windows;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for ProfileViewControl.xaml
    /// </summary>
    public partial class ProfileViewControl : UserControl
    {
        private UserData _userData;
        private Client _client;
        private User _user;
        private Winery _winery;
        private List<Wine> _wines; 

        public ProfileViewControl(UserData userData, User user, Winery winery, List<Wine> wines, Client client)
        {
            InitializeComponent();
            _userData = userData;
            _user = user;
            _winery = winery;
            _wines = wines;
            _client = client;
            PopulateViewControl();

        }

        private void PopulateViewControl()
        {
            TextBlockUsername.Text = _user.UserName;

            if (!String.IsNullOrEmpty(_user.PicturePath))
            {
                Image.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(_user.PicturePath);
            }

            if (_user.Status != null)
            {
                TextBlockStatus.Text = _user.Status;
            }

            if (_user.FirstName != null)
            {
                TextBlockFirstName.Text = _user.FirstName;
            }

            if (_user.LastName != null)
            {
                TextBlockLastName.Text = _user.LastName;
            }

            if (_user.PhoneNumber != null)
            {
                TextBlockPhoneNumber.Text = _user.PhoneNumber;
            }

            if (_user.Email != null)
            {
                TextBlockEmail.Text = _user.Email;
            }

            if (_winery != null)
            {
                TextBlockWineryName.Text = _winery.Name;
                TextBlockAddress.Text = _winery.Address;
            }

            ListViewWine.ItemsSource = _wines;
        }

        private void ListViewWine_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListViewWine.SelectedItem != null && !string.IsNullOrEmpty(((Wine)ListViewWine.SelectedItem).PicturePath))
            {
                ImageWine.Source = (ImageSource) new ImageSourceConverter().ConvertFromString(((Wine)ListViewWine.SelectedItem).PicturePath);
            }
            else if (ListViewWine.SelectedItem != null)
            {
                ImageWine.Source = null;
            }
        }

        private void BtnBlock_Click(object sender, RoutedEventArgs e)
        {
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.ServerRequestType = ServerRequestType.BlockUser;
            serverRequest.User = _userData.User;
            serverRequest.UserId = _user.Id;
            serverRequest.BlockUserType = true;

            _client.SendRequestUserControl(this, serverRequest);
            //TODO : blokiranje
        }

        private void BtnSendMessage_Click(object sender, RoutedEventArgs e)
        {
            DirectMessageWindow messageWindow = new DirectMessageWindow(this, _userData, _user, _client);
            messageWindow.Show();
        }
         public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.ServerNotAvailable)
                {
                    MessageBoxResult result = MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    if (serverRequest.ServerRequestType == ServerRequestType.BlockUser)
                    {
                        TextBlockWarning.Foreground = Brushes.Green;
                        TextBlockWarning.Text = serverRequest.Message;
                    }
                    else if (serverRequest.ServerRequestType == ServerRequestType.SendDirectMessage)
                    {
                        TextBlockWarning.Foreground = Brushes.Green;
                        TextBlockWarning.Text = serverRequest.Message;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(serverRequest.Message))
                        {
                            TextBlockWarning.Text = serverRequest.Message;
                            TextBlockWarning.Foreground = Brushes.Red;
                        }
                        else
                        {
                            TextBlockWarning.Text = "Poslani zahtjevi za stvaranje razgovora!";
                            TextBlockWarning.Foreground = Brushes.Green;
                        }
                    }
                }
            }));
        }

        private void BtnUnBlock_OnClick(object sender, RoutedEventArgs e)
        {
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.ServerRequestType = ServerRequestType.BlockUser;
            serverRequest.User = _userData.User;
            serverRequest.UserId = _user.Id;
            serverRequest.BlockUserType = false;

            _client.SendRequestUserControl(this, serverRequest);
        }
    }
}
