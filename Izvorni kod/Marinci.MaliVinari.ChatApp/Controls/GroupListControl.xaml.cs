﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;
using Marinci.MaliVinari.ChatApp.Windows;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for GroupListControl.xaml
    /// </summary>
    public partial class GroupListControl : UserControl
    {
        private UserData _userData;
        private Panel _panelControl;
        private Client _client;
        private UserInterfaceWindow _userInterface;

        public GroupListControl(UserData userData,Panel panelControl, Client client, UserInterfaceWindow userInterface)
        {
            InitializeComponent();
            _client = client;
            _userData = userData;
            _panelControl = panelControl;
            _userInterface = userInterface;
            ListViewGroups.ItemsSource = _userData.Groups;
        }

        private void ListViewGroups_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //TODO refreshati da se vidi ko je sve online

            _panelControl.Children.Clear();
            _panelControl.Children.Add(new GroupViewControl(_userData, _client, (Group) ListViewGroups.SelectedItem, _panelControl, _userInterface));
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.GroupCreated)
                {
                    _userData.Groups.Add(serverRequest.Group);

                    ListViewGroups.ClearValue(ItemsControl.ItemsSourceProperty);
                    ListViewGroups.ItemsSource = _userData.Groups;
                }
                else if (serverRequest.ServerRequestType == ServerRequestType.GroupClosed)
                {
                    _userData.Groups.Remove(serverRequest.Group);

                    ListViewGroups.ClearValue(ItemsControl.ItemsSourceProperty);
                    ListViewGroups.ItemsSource = _userData.Groups;
                }
                else if (serverRequest.ServerRequestType == ServerRequestType.EndConversation)
                {
                    _userInterface.EnableElements();

                    _userInterface.PanelControl.Children.Clear();
                    _userInterface.PanelControl.Children.Add(new GroupViewControl(_userData, _client,
                        serverRequest.Group, _panelControl, _userInterface));
                }
            }));
        }
    }
}
