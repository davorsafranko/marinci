﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;
using Marinci.MaliVinari.ChatApp.Windows;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for ConversationControl.xaml
    /// </summary>
    public partial class ConversationControl : UserControl
    {
        private UserData _userData;
        private Client _client;
        private Group _group;
        private StringBuilder _sbBuilder;
        private UserInterfaceWindow _window;
        public ConversationControl(UserData userData, Client client, Group group, String message, Conversation conversation, UserInterfaceWindow userInterface)
        {
            InitializeComponent();
            _userData = userData;
            _client = client;
            _sbBuilder = new StringBuilder();
            _sbBuilder.Append(message);
            _userData.CurrentConversation = conversation;
            _window = userInterface;
            _group = group;
            _window.DisableElements();
            TextBlockMessages.Text = _sbBuilder.ToString();
        }


        private void BtnSend_OnClick(object sender, RoutedEventArgs e)
        {
            if (TextBoxMessage.Text != "")
            {
                ServerRequest serverRequest = new ServerRequest();
                serverRequest.ServerRequestType = ServerRequestType.SendMessage;
                serverRequest.User = _userData.User;
                serverRequest.Group = _group;
                serverRequest.Conversation = _userData.CurrentConversation;
                serverRequest.Message = _userData.User.GetUserTitle() + " : " + TextBoxMessage.Text;
                TextBoxMessage.Text = "";
                
                _client.SendRequestUserControl(this, serverRequest);
            }
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.SendMessage)
                {
                    if (!String.IsNullOrEmpty(serverRequest.Message))
                    {
                        _sbBuilder.Append(serverRequest.Message);
                        _sbBuilder.AppendLine();
                        TextBlockMessages.Text = _sbBuilder.ToString();
                        ScrollViewerMessages.ScrollToBottom();
                    }
                    else
                    {
                        MessageBox.Show("Pogreška prilikom slanja poruke!", "Obavijest", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                }
            }));
        }

        private void BtnFinishConversation_OnClick(object sender, RoutedEventArgs e)
        {
            ServerRequest serverRequest = new ServerRequest();

            serverRequest.ServerRequestType = ServerRequestType.EndConversation;
            serverRequest.User = _userData.User;
            serverRequest.Group = _group;
            serverRequest.Conversation = _userData.CurrentConversation;

            _client.SendRequestUserControl(this, serverRequest);
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible == false)
            {
                _userData.CurrentConversation = null;
            }
        }
    }
}
