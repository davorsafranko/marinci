﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;
using Microsoft.Win32;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for ProfileEditControl.xaml
    /// </summary>
    public partial class ProfileEditControl : UserControl
    {
        private Client _client;
        private UserData _userData;
        private FileDialog fileDialog;
        private WineEditingControl _wineEditingControl;

        public ProfileEditControl(Client client, UserData userData)
        {
            InitializeComponent();

            _client = client;
            _userData = userData;
            _wineEditingControl = new WineEditingControl(_userData);
            WineEditingPanel.Children.Add(_wineEditingControl);
            PopulateEditControl();
        }

        private void PopulateEditControl()
        {
            TextBlockUsername.Text = _userData.User.UserName;

            if (!String.IsNullOrEmpty(_userData.User.PicturePath))
            {
                Image.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(_userData.User.PicturePath);
            }

            if (_userData.User.Status != null)
            {
                TextBoxStatus.Text = _userData.User.Status;
            }

            if (_userData.User.FirstName != null)
            {
                TextBoxFirstName.Text = _userData.User.FirstName;
            }

            if (_userData.User.LastName != null)
            {
                TextBoxLastName.Text = _userData.User.LastName;
            }

            if (_userData.User.PhoneNumber != null)
            {
                TextBoxPhoneNumber.Text = _userData.User.PhoneNumber;
            }
            
            if (_userData.User.Email != null)
            {
                TextBoxEmail.Text = _userData.User.Email;
            }

            if (_userData.Winery != null)
            {
                TextBoxWineryName.Text = _userData.Winery.Name;
                TextBoxAddress.Text = _userData.Winery.Address;
            }

            List<Wine> wines = _userData.Wines;

            _wineEditingControl.ListViewWine.ItemsSource = wines;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (fileDialog != null && !String.IsNullOrEmpty(fileDialog.FileName))
                {
                    _userData.User.PicturePath = CommonMethods.UploadImage(fileDialog.FileName);
                }
                else if (Image.Source == null)
                {
                    _userData.User.PicturePath = null;
                }

                if (TextBoxStatus.Text != "")
                {
                    _userData.User.Status = TextBoxStatus.Text;
                }

                if (PasswordBox.Password != "")
                {
                    _userData.User.PasswordHash = PasswordBox.Password;
                }

                if (TextBoxFirstName.Text != "")
                {
                    _userData.User.FirstName = TextBoxFirstName.Text;
                }

                if (TextBoxLastName.Text != "")
                {
                    _userData.User.LastName = TextBoxLastName.Text;
                }

                if (TextBoxPhoneNumber.Text != "")
                {
                    _userData.User.PhoneNumber = TextBoxPhoneNumber.Text;
                }

                if (TextBoxEmail.Text != "")
                {
                    _userData.User.Email = TextBoxEmail.Text;
                }

                if (TextBoxWineryName.Text != "")
                {
                    _userData.Winery.Name = TextBoxWineryName.Text;
                }

                if (TextBoxAddress.Text != "")
                {
                    _userData.Winery.Address = TextBoxAddress.Text;
                }

                ServerRequest serverRequest = new ServerRequest();
                serverRequest.User = _userData.User;
                serverRequest.Winery = _userData.Winery;
                serverRequest.Wines = _wineEditingControl.GetWineList();
                serverRequest.ServerRequestType = ServerRequestType.UpdateUser;

                //uploadaj slike vine

                foreach (Wine wine in serverRequest.Wines)
                {
                    if (!String.IsNullOrEmpty(wine.PicturePath))
                    {
                        if (!wine.PicturePath.StartsWith("http"))
                        {
                            wine.PicturePath = CommonMethods.UploadImage(wine.PicturePath);
                        }
                    }
                }

                _client.SendRequestUserControl(this, serverRequest);
            }
            catch (Exception)
            {
            }
        }

        private void BtnSetImage_Click(object sender, RoutedEventArgs e)
        {
            fileDialog = new OpenFileDialog();
            fileDialog.ShowDialog();
            if (!String.IsNullOrEmpty(fileDialog.FileName))
            { 
                Image.Source = (ImageSource) new ImageSourceConverter().ConvertFromString(fileDialog.FileName);
            }
        }


        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.ServerNotAvailable)
                {
                    MessageBoxResult result = MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (serverRequest.ServerRequestType == ServerRequestType.UpdateUser &&
                    String.IsNullOrEmpty(serverRequest.Message))
                {
                    _userData.User = serverRequest.User;
                    _userData.Winery = serverRequest.Winery;
                    _userData.Wines = serverRequest.Wines;
                    TextBlockWarning.Text = "Postavke uspješno promijenjene!";
                    TextBlockWarning.Foreground = Brushes.Green;
                }
                else
                {
                    TextBlockWarning.Text = "Postavke nisu uspješno promijenjene!";
                    TextBlockWarning.Foreground = Brushes.Red;
                }
            }));
        }

        private void BtnRemoveImage_OnClick(object sender, RoutedEventArgs e)
        {
            Image.Source = null;
        }
    }
}
