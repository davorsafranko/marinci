﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for RequestControl.xaml
    /// </summary>
    public partial class RequestControl : UserControl
    {
        private UserData _userData;
        private Client _client;
        public RequestControl(UserData userData, Client client)
        {
            InitializeComponent();
            _userData = userData;
            _client = client;
            ListBoxRequests.ItemsSource = _userData.Requests;
        }

        private void ListBoxRequests_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Request selectedRequest = ListBoxRequests?.SelectedItem as Request;
            if (selectedRequest != null)
            {
                MessageBoxResult result = MessageBox.Show(selectedRequest.RequestMessage, "Zahtjev",
                    MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    selectedRequest.Accepted = true;
                }
                else
                {
                    selectedRequest.Accepted = false;
                }

                selectedRequest.IsRead = true;
            }

            ListBoxRequests?.ClearValue(ItemsControl.ItemsSourceProperty);
            _userData.Requests.Remove(selectedRequest);

            if (ListBoxRequests != null) ListBoxRequests.ItemsSource = _userData.Requests;

            //javiti serveru da je odgovoreno

            ServerRequest serverRequest = new ServerRequest();
            serverRequest.User = _userData.User;
            serverRequest.ServerRequestType = ServerRequestType.RequestAnswered;
            serverRequest.RequestAnswer = selectedRequest;

            _client.SendRequestUserControl(this, serverRequest);
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType != ServerRequestType.ServerNotAvailable)
                {
                    _userData.Requests.Add(serverRequest.SendRequest);
                    ListBoxRequests.ClearValue(ItemsControl.ItemsSourceProperty);
                    ListBoxRequests.ItemsSource = _userData.Requests;
                }
                else
                {
                    MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }));
        }
    }
}
