﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;
using Marinci.MaliVinari.ChatApp.Windows;
using UserControl = System.Windows.Controls.UserControl;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for NotificationControl.xaml
    /// </summary>
    public partial class NotificationControl : UserControl
    {
        private UserData _userData;
        private Client _client;

        public NotificationControl(UserData userData, Client client)
        {
            InitializeComponent();
            _userData = userData;
            _client = client;
            ListBoxNotifications.ItemsSource = _userData.Notifications;
        }

        private void ListBoxNotifications_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Notification selectedNotification = ListBoxNotifications?.SelectedItem as Notification;
            if (selectedNotification != null)
                MessageBox.Show(selectedNotification.Message, "Obavijest", MessageBoxButton.OK,
                    MessageBoxImage.Information);

            if (ListBoxNotifications != null)
            {
                ListBoxNotifications.ClearValue(ItemsControl.ItemsSourceProperty);

                _userData.Notifications.Remove(selectedNotification);

                //javiti serveru da je pročitano

                ListBoxNotifications.ItemsSource = _userData.Notifications;

                ServerRequest serverRequest = new ServerRequest();
                serverRequest.User = _userData.User;
                serverRequest.ServerRequestType = ServerRequestType.NotificationRead;
                if (selectedNotification != null) serverRequest.NotificationReadId = selectedNotification.Id;

                _client.SendRequestUserControl(this, serverRequest);
            }
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType != ServerRequestType.ServerNotAvailable)
                {
                    _userData.Notifications.Add(serverRequest.Notification);
                    ListBoxNotifications.ClearValue(ItemsControl.ItemsSourceProperty);
                    ListBoxNotifications.ItemsSource = _userData.Notifications;
                }
                else
                {
                    MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }));
        }
    }
}
