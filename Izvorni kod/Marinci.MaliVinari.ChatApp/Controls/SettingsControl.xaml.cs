﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;
using Newtonsoft.Json.Converters;

namespace Marinci.MaliVinari.ChatApp.Controls
{
    /// <summary>
    /// Interaction logic for SettingsControl.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        private Client _client;
        private User _user;
        private Window _window;

        public SettingsControl(Client client, User user, Window window)
        {
            InitializeComponent();
            _client = client;
            _user = user;
            _window = window;

            TextBoxIpAddress.Text = _client.GetServerIpEndPoint().Address.ToString();
            TextBoxPort.Text = _client.GetServerIpEndPoint().Port.ToString();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            TextBlockWarning.Text = "";
            string color = "";
            if (ComboBox.SelectedItem != null)
            {
                color = ((ComboBoxItem) ComboBox.SelectedItem).Name;
                switch (((ComboBoxItem)ComboBox.SelectedItem).Name)
                {
                    case "RedColor":
                        _window.Background = Brushes.LightCoral;
                        break;
                    case "BlueColor":
                        _window.Background = Brushes.LightBlue;
                        break;
                    case "GreenColor":
                        _window.Background = Brushes.DarkSeaGreen;
                        break;
                    case "BeigeColor":
                        _window.Background = Brushes.Beige;
                        break;
                    default:
                        _window.Background = Brushes.White;
                        break;
                }
            }
            try
            {
                ServerRequest serverRequest = new ServerRequest();
                serverRequest.ServerRequestType = ServerRequestType.ChangeApplicationParameters;
                serverRequest.User = _user;

                //ovdje treba napuniti parametre
                ApplicationParameters applicationParameters = new ApplicationParameters();
                if (TextBoxIpAddress.Text != "")
                {
                    IPAddress ipAddress;
                    IPAddress.TryParse(TextBoxIpAddress.Text, out ipAddress);

                    if (ipAddress == null)
                    {
                        throw new FormatException();
                    }

                    applicationParameters.IpAddress = TextBoxIpAddress.Text;
                }
                else
                {
                    applicationParameters.IpAddress = _client.GetServerIpEndPoint().Address.ToString();
                }

                if (TextBoxPort.Text != "")
                {
                    applicationParameters.Port = int.Parse(TextBoxPort.Text);
                }
                else
                {
                    applicationParameters.Port = _client.GetServerIpEndPoint().Port;
                }

                if (!String.IsNullOrEmpty(color))
                {
                    applicationParameters.SkinColor = color;
                }

                serverRequest.ApplicationParameters = applicationParameters;
                //promjena kod klijenta 
                _client.SetServerParameters(serverRequest.ApplicationParameters);
                //pohrana podatak u bazi podataka
                _client.SendRequestUserControl(this, serverRequest);
            }
            catch (FormatException ex)
            {
                TextBlockWarning.Foreground = Brushes.Red;
                TextBlockWarning.Text = "Nevaljana IP adresa!";
            }
        }

        public void ProcessResponseFromServer(ServerRequest serverRequest)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                if (serverRequest.ServerRequestType == ServerRequestType.ServerNotAvailable)
                {
                    MessageBoxResult result = MessageBox.Show("Poslužitelj nije dostupan!", "Pogreška",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    //sve dobro
                    TextBlockWarning.Foreground = Brushes.Green;
                    TextBlockWarning.Text = "Parametri uspješno promijenjeni!";
                }
            }));

        }
    }
}
