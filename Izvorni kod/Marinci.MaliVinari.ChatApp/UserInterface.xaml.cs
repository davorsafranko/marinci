﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Models;
using ListViewItem = System.Windows.Forms.ListViewItem;

namespace Marinci.MaliVinari.ChatApp
{
    /// <summary>
    /// Interaction logic for UserInterface.xaml
    /// </summary>
    public partial class UserInterface : Window
    {
        private UserData _userData;
        private Client _client;

        public UserInterface(UserData userData, Client client)
        {
            InitializeComponent();
            _userData = userData;
            _client = client;
            PanelControl.Children.Add(new UserProfileParentControl());
            PopulateGroupList();
        }

        private void PopulateGroupList()
        {
            //TODO: dohvati grupe iz baze
            List<GroupControl> groupList = new List<GroupControl>();

            ListViewGroups.ItemsSource = groupList;

        }

        private void ListViewGroups_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListViewGroups.SelectedItem != null)
            {
                PanelControl.Children.Clear();
                PanelControl.Children.Add((UIElement)ListViewGroups.SelectedItem);
            }
        }

        private void TextBoxSearch_OnMouseEnter(object sender, MouseEventArgs e)
        {
            TextBoxSearch.Text = "";
            TextBoxSearch.Foreground = SystemColors.WindowTextBrush;
        }

        private void TextBoxSearch_OnGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBoxSearch.Text = "";
            TextBoxSearch.Foreground = SystemColors.WindowTextBrush;
        }

        private void TextBoxSearch_OnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            TextBoxSearch.Text = "Trazi";
            TextBoxSearch.Foreground = SystemColors.GrayTextBrush;
        }

        private void BtnMyProfile_OnClick(object sender, RoutedEventArgs e)
        {
            PanelControl.Children.Clear();
            PanelControl.Children.Add(new UserProfileEditControl());
        }

        private void BtnSettings_Click(object sender, RoutedEventArgs e)
        {
            PanelControl.Children.Clear();
            PanelControl.Children.Add(new SettingsControl());
        }

        private void BtnCreateGroup_Click(object sender, RoutedEventArgs e)
        {
            PanelControl.Children.Clear();
            PanelControl.Children.Add(new AddGroupControl());
        }
    }
}
