﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;

namespace Marinci.MaliVinari.ChatApp.Models
{
    public class UserData
    {
        public User User;
        public Winery Winery;
        public List<Wine> Wines;
        public List<Group> Groups;
        public List<Notification> Notifications;
        public List<Request> Requests;
        public ApplicationParameters ApplicationParameters;
        public Conversation CurrentConversation;

        //Dodati sve podatke o useru koji su potrebni u glavnom sučelju
        public UserData(User user, Winery winery, List<Wine> wines,List<Group> groups, List<Notification> notifications, List<Request> requests, ApplicationParameters applicationParameters)
        {
            User = user;
            Winery = winery;
            Wines = wines;
            Groups = groups;
            Notifications = notifications;
            Requests = requests;
            ApplicationParameters = applicationParameters;
        }

        public User GetUser()
        {
            return this.User;
        }
    }
}
