﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Common;
using Marinci.MaliVinari.Business.Models;
using Marinci.MaliVinari.ChatApp.Controls;
using Marinci.MaliVinari.ChatApp.Models;
using Marinci.MaliVinari.ChatApp.Windows;
using Newtonsoft.Json;

namespace Marinci.MaliVinari.ChatApp
{
    public class Client
    {
        private volatile IPEndPoint _serverIpEndPoint;
        private IPEndPoint _localIpEndPoint;
        private Dictionary<int ,UserControl>_userControls;
        private Dictionary<int, System.Windows.Window> _windows; 
        private readonly Socket _listenerSocket;
        private Thread _listenerThread;
        private volatile bool _listenServer = false;
        private const int BufferSize = 65536;
        private static int _userControlId = 4;
        private static int _windowId = 0;

        public Client(string serverIpAddress, int port)
        {
            _listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            SetLocalIpEndPoint();

            _serverIpEndPoint = new IPEndPoint(IPAddress.Parse(serverIpAddress), port);
            _userControls = new Dictionary<int, UserControl>();
            _windows = new Dictionary<int, Window>();
        }

        public void StartClient()
        {
            _listenServer = true;

            _listenerSocket.Listen(1); 

            _listenerThread = new Thread(Listen);
            _listenerThread.Start();
        }

        private void Listen()
        {
            while (_listenServer)
            {
                Socket serverSocket = _listenerSocket.Accept();
                if (_listenServer)
                {
                    Task.Run(() => ProcessResponse(serverSocket));
                }
                else
                {
                    break;
                }
            }
        }

        private void ProcessResponse(Socket serverSocket)
        {
            string receivedData = "";
            byte[] receiveBuffer = new byte[BufferSize];

            while (true)
            {
                int receivedBytes = serverSocket.Receive(receiveBuffer);
                receivedData += Encoding.UTF8.GetString(receiveBuffer, 0, receivedBytes);

                if (receivedBytes < BufferSize)
                {
                    break;
                }
            }

            ServerRequest serverRequest = JsonConvert.DeserializeObject<ServerRequest>(receivedData);

            //pročitaj iz zahtjeva što napraviti s pristiglim podacima

            ForwardResponseDataToCaller(serverRequest);

            serverSocket.Close();
        }

        private void ForwardResponseDataToCaller(ServerRequest serverRequest)
        {
            //userControl
            if (serverRequest.WindowId == -1)
            {
                if (_userControls[serverRequest.UserControlId].GetType() == typeof(CreateGroupControl))
                {
                    CreateGroupControl cgControl = (CreateGroupControl)_userControls[serverRequest.UserControlId];
                    cgControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof(SettingsControl))
                {
                    SettingsControl sControl = (SettingsControl)_userControls[serverRequest.UserControlId];
                    sControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof (ProfileEditControl))
                {
                    ProfileEditControl peControl = (ProfileEditControl) _userControls[serverRequest.UserControlId];
                    peControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof (SearchUserControl))
                {
                    SearchUserControl suControl = (SearchUserControl) _userControls[serverRequest.UserControlId];
                    suControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof (ProfileViewControl))
                {
                    ProfileViewControl pwControl = (ProfileViewControl) _userControls[serverRequest.UserControlId];
                    pwControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof(RequestControl))
                {
                    RequestControl rControl = (RequestControl)_userControls[serverRequest.UserControlId];
                    rControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof(NotificationControl))
                {
                    NotificationControl nControl = (NotificationControl)_userControls[serverRequest.UserControlId];
                    nControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof(GroupListControl))
                {
                    GroupListControl glControl = (GroupListControl)_userControls[serverRequest.UserControlId];
                    glControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof(GroupViewControl))
                {
                    GroupViewControl gwControl = (GroupViewControl)_userControls[serverRequest.UserControlId];
                    gwControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof(ConversationControl))
                {
                    ConversationControl cControl = (ConversationControl)_userControls[serverRequest.UserControlId];
                    cControl.ProcessResponseFromServer(serverRequest);
                }
                else if (_userControls[serverRequest.UserControlId].GetType() == typeof(ConversationHistroyControl))
                {
                    ConversationHistroyControl chControl = (ConversationHistroyControl)_userControls[serverRequest.UserControlId];
                    chControl.ProcessResponseFromServer(serverRequest);
                }
            }
            //window
            else
            {
                if (_windows[serverRequest.WindowId].GetType() == typeof(LoginWindow))
                {
                    LoginWindow window = (LoginWindow)_windows[serverRequest.WindowId];
                    window.ProcessResponseFromServer(serverRequest);
                }
                else if (_windows[serverRequest.WindowId].GetType() == typeof(RegisterWindow))
                {
                    RegisterWindow window = (RegisterWindow)_windows[serverRequest.WindowId];
                    window.ProcessResponseFromServer(serverRequest);
                }
            }
        }

        public void SendRequestUserControl(UserControl userControl, ServerRequest serverRequest)
        {
            if (userControl.GetType() != typeof (ConversationControl))
            {
                serverRequest.UserControlId = GetNextUserControlId();
                _userControls[serverRequest.UserControlId] = userControl;
            }
            else
            {
                serverRequest.UserControlId = Constants.ConversationControId;
            }
            Task.Run(() => SendRequestAsync(serverRequest));
        }

        public void SendRequestWindows(System.Windows.Window window, ServerRequest serverRequest)
        {
            serverRequest.WindowId = GetNextWindowId();
            _windows[serverRequest.WindowId] = window;
            Task.Run(() => SendRequestAsync(serverRequest));
        }

        private void SendRequestAsync(ServerRequest serverRequest)
        {
            try
            {
                byte[] request = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(serverRequest));

                Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Connect(_serverIpEndPoint);

                int sent = serverSocket.Send(request);

                serverSocket.Close();
            }
            catch (SocketException ex)
            {
                serverRequest.ServerRequestType = ServerRequestType.ServerNotAvailable;
                ForwardResponseDataToCaller(serverRequest);
            }
        }

        private void SetLocalIpEndPoint()
        {
            int i = 50000;

            while (true)
            {
                var ipEndPoint = new IPEndPoint(IPAddress.Parse(CommonMethods.GetLocalIPAddress()), i);

                try
                {
                    _listenerSocket.Bind(ipEndPoint);
                    _localIpEndPoint = ipEndPoint;
                    break;
                }
                catch
                {
                    i++;
                }
            }
        }

        private int GetNextWindowId()
        {
            return _windowId++;
        }

        private int GetNextUserControlId()
        {
            return _userControlId++;
        }

        public IPEndPoint GetClientEndPoint()
        {
            return this._localIpEndPoint;
        }

        public void Shutdown()
        {
            try
            {
                _listenServer = false;
                byte[] request = new byte[] {};

                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(_localIpEndPoint);

                int sent = socket.Send(request);

                socket.Close();
            }
            catch (SocketException ex)
            {
                //ne treba raditi ništa, znači da je već ugašeno slušanje poruka od servera
            }
        }

        public void SetServerParameters(ApplicationParameters parameters)
        {
            _serverIpEndPoint.Port = parameters.Port;
            _serverIpEndPoint.Address = IPAddress.Parse(parameters.IpAddress);
        }

        public IPEndPoint GetServerIpEndPoint()
        {
            return _serverIpEndPoint;
        }

        public void AddUserControl(UserControl userControl)
        {
            if (userControl.GetType() == typeof (NotificationControl))
            {
                _userControls[Constants.NotificationControlId] = userControl;
            }
            else if(userControl.GetType() == typeof(RequestControl))
            {
                _userControls[Constants.RequestControlId] = userControl;
            }
            else if (userControl.GetType() == typeof (GroupListControl))
            {
                _userControls[Constants.GroupControlId] = userControl;
            }
            else if (userControl.GetType() == typeof(ConversationControl))
            {
                _userControls[Constants.ConversationControId] = userControl;
            }
        }

        public void UpdateGroupList(UserData userData)
        {
            GroupListControl gcControl = (GroupListControl)_userControls[Constants.GroupControlId];
            gcControl.ListViewGroups.ClearValue(ItemsControl.ItemsSourceProperty);
            gcControl.ListViewGroups.ItemsSource = userData.Groups;
        }
    }
}
