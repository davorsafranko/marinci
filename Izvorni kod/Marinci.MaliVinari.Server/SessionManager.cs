﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Marinci.MaliVinari.Server
{
    public class SessionManager
    {
        private Dictionary<int, IPEndPoint> _connectedUsers;
        private Dictionary<int, HashSet<int>> _groupInfo;
        private int _maxNumberOfUsers;

        public SessionManager(int maxNumberOfUsers)
        {
            _maxNumberOfUsers = maxNumberOfUsers;
            _connectedUsers = new Dictionary<int, IPEndPoint>();
            _groupInfo = new Dictionary<int, HashSet<int>>();
        }

        public bool AddUserInfo(int userId, string IPAddress, int port)
        {
            if (_connectedUsers.Count == _maxNumberOfUsers)
            {
                return false;
            }
            else
            {
                if (!_connectedUsers.ContainsKey(userId) && userId != -1)
                {
                    IPEndPoint userEndPoint = new IPEndPoint(System.Net.IPAddress.Parse(IPAddress), port);
                    _connectedUsers.Add(userId, userEndPoint);
                }

                return true;
            }
        }

        public IPEndPoint GetUserEndPointInfo(int userId)
        {
            return _connectedUsers[userId];
        }

        public HashSet<int> GetGroupUsers(int groupId)
        {
            return _groupInfo[groupId];
        }

        public void AddUserToGroup(int groupId, int userId)
        {
            if (!_groupInfo.ContainsKey(groupId))
            {
                _groupInfo[groupId] = new HashSet<int>();
            }
            _groupInfo[groupId].Add(userId);
        }

        public void RemoveUserFromConnectedUsers(int userId)
        {
            _connectedUsers.Remove(userId);
        }

        public void RemoveGroup(int groupId)
        {
            _groupInfo.Remove(groupId);
        }

        public bool ContainsUserId(int userId)
        {
            return this._connectedUsers.ContainsKey(userId);
        }

        public void AddGroup(int groupId, HashSet<int> userIds)
        {
            if (!_groupInfo.ContainsKey(groupId))
            {
                _groupInfo.Add(groupId, userIds);
            }
        }

        public List<int> GetAllConnectedUsers()
        {
            return this._connectedUsers.Keys.ToList();
        }

        public int GetNumberOfLoggedInUsers()
        {
            return this._connectedUsers.Count;
        }
    }
}
