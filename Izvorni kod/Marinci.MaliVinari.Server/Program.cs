﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Models;

namespace Marinci.MaliVinari.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ovdje treba iz baze dohvatiti port na kojem server slusa konekcije
            ServerParametersRepository serverParametersRepository = new ServerParametersRepository();
            ServerParameters serverParameters = serverParametersRepository.GetServerParameters();

            if (serverParameters == null)
            {
                serverParameters = new ServerParameters(50001, 10);
            }
            Server server = new Server(CommonMethods.GetLocalIPAddress(), serverParameters);
            server.StartServer();
        }
    }
}
