﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Common;
using Marinci.MaliVinari.Business.Models;

namespace Marinci.MaliVinari.Server
{
    public class Dispatcher
    {
        private SessionManager _sessionManager;
        private Server _server;

        public Dispatcher(SessionManager sessionManager, Server server)
        {
            _sessionManager = sessionManager;
            _server = server;
        }

        public void GetUserByUserName(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            User user = userRepository.GetUserByUserName(serverRequest.User.UserName);

            serverRequest.User = user;

            if (user == null)
            {
                return;
            }

            WineryRepository wineryRepository = new WineryRepository();
            Winery winery = wineryRepository.GetUserWinery(user.Id);

            serverRequest.Winery = winery;

            if (winery != null)
            {
                List<Wine> wines = new List<Wine>();

                WineRepository wineRepository = new WineRepository();
                wines = wineRepository.GetWineryWines(winery.Id);

                serverRequest.Wines = wines;
            }
            else
            {
                serverRequest.Wines = null;
            }

            NotificationRepository notificationRepository = new NotificationRepository();
            serverRequest.Notifications = notificationRepository.GetUsersNotifications(serverRequest.User.Id);

            RequestRepository requestRepository = new RequestRepository();
            serverRequest.Requests = requestRepository.GetUsersRequests(serverRequest.User.Id);

            GroupRepository groupRepository = new GroupRepository();
            serverRequest.Groups = groupRepository.GetUserGroups(serverRequest.User.Id).ToList();

            ApplicationParametersRepository applicationParametersRepository = new ApplicationParametersRepository();
            ApplicationParameters applicationParameters =
                applicationParametersRepository.GetParametersByUserId(serverRequest.User.Id);

            serverRequest.ApplicationParameters = applicationParameters;

            SetUserLoggedInStatus(serverRequest, true);
        }

        public void RegisterUser(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            User user = serverRequest.User;
            
            if (userRepository.GetUserByUserName(user.UserName) == null &&
                userRepository.GetUserByEmail(user.Email) == null)
            {
                userRepository.Insert(user);
            }
            else
            {
                serverRequest.User = new User();
                serverRequest.Message = "Korisničko ime ili e-mail su već zauzeti.";
                return;
            }

            if (serverRequest.Winery != null)
            {
                serverRequest.Winery.UserId = serverRequest.User.Id;

                WineryRepository wineryRepository = new WineryRepository();
                wineryRepository.Insert(serverRequest.Winery);

                if (serverRequest.Wines != null)
                {
                    WineRepository wineRepository = new WineRepository();

                    foreach (Wine wine in serverRequest.Wines)
                    {
                        wine.WineryId = serverRequest.Winery.Id;
                        wineRepository.Insert(wine);
                    }
                }
            }
        }

        public void CreateGroup(ServerRequest serverRequest)
        {
            List<User> users = new List<User>();

            UserRepository userRepository = new UserRepository();

            GroupRepository groupRepository = new GroupRepository();
            Group group = new Group();

            group.GroupName = serverRequest.GroupRequestModel.GroupName;
            group.IsGroupChat = serverRequest.GroupRequestModel.IsGroupChat;
            group.IsVisible = false;

            groupRepository.Insert(group);

            foreach (int userId in serverRequest.GroupRequestModel.UserIds)
            {
                _sessionManager.AddUserToGroup(group.Id, userId);
            }

            _sessionManager.AddUserToGroup(group.Id, serverRequest.User.Id);

            foreach (int groupUser in _sessionManager.GetGroupUsers(group.Id))
            {
                users.Add(userRepository.GetById(groupUser));
            }

            serverRequest.GroupId = group.Id;

            if (userRepository.CanFormGroup(users))
            {
                SaveRequests(group.Id, GroupRequestType.CreateGroup, serverRequest.User.Id);
            }
            else
            {
                serverRequest.Message = "Grupa se ne može kreirati jer se netko od vinara međusobno blokira.";
                _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(serverRequest.User.Id));
            }
        }

        private void SaveRequests(int groupId, GroupRequestType groupRequestType, int idSender, int conversationId = -1)
        {
            RequestRepository requestRepository = new RequestRepository();
            //poslati zahtjeve za stvaranje grupe
            foreach (int userId in _sessionManager.GetGroupUsers(groupId))
            {
                if (userId != idSender)
                {
                    Request request = new Request();
                    request.GroupID = groupId;
                    request.UserId = userId;
                    request.IsRead = false;
                    request.Accepted = false;
                    request.RequestType = (int)groupRequestType;//0 je kreiranje grupe

                    if (conversationId != -1)
                    {
                        request.ConversationId = conversationId;
                    }

                    requestRepository.Insert(request);

                    UserRepository userRepository = new UserRepository();
                    User user = userRepository.GetById(userId);

                    SendGroupRequest(request, user, idSender);
                }
            }
        }

        public void GetWinemakers(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            if (String.IsNullOrEmpty(serverRequest.SearchText))
            {
                serverRequest.Winemakers = userRepository.GetAllWinemakers(serverRequest.User.Id);
            }
            else
            {
                serverRequest.Winemakers = userRepository.Search(serverRequest.SearchText).ToList();
            }

            SetUsersSignedInStatus(serverRequest);
            
        }

        public void ShutDownServer(ServerRequest serverRequest)
        {
            //TODO poslati poruku svim korisnicima o prekidu rada poslužitelja i provjera za admina!

            UserRepository userRepository = new UserRepository();
            User user = userRepository.GetById(serverRequest.User.Id);

            if (user.RoleId == 1)
            {
                foreach (int connectedUser in _sessionManager.GetAllConnectedUsers())
                {
                    Notification notification = new Notification();
                    notification.UserId = connectedUser;
                    notification.IsRead = false;
                    notification.Message = "Administrator je pokrenuo gašenje poslužitelja.";

                    SendNotification(userRepository.GetById(connectedUser), notification);
                }
                Environment.Exit(1);
            }
        }

        public void LogOutUser(ServerRequest serverRequest)
        {
            _sessionManager.RemoveUserFromConnectedUsers(serverRequest.User.Id);
            SetUserLoggedInStatus(serverRequest, false);

            if (serverRequest.Conversation != null)
            {
                UserInGroupRepository userInGroupRepository = new UserInGroupRepository();
                UserRepository userRepository = new UserRepository();
                GroupRepository groupRepository = new GroupRepository();
                NotificationRepository notificationRepository = new NotificationRepository();
                RequestRepository requestRepository = new RequestRepository();
                ConversationRepository conversationRepository = new ConversationRepository();

                List<int> groupUsers = userInGroupRepository.GetGroupUserIds(serverRequest.Conversation.GroupId);
                Group group = groupRepository.GetById(serverRequest.Conversation.GroupId);
                User caller = userRepository.GetById(serverRequest.User.Id);
                Conversation receivedConversation = serverRequest.Conversation;
                foreach (int groupUserId in groupUsers)
                {
                    if (groupUserId != caller.Id)
                    {
                        Notification notification = new Notification();
                        notification.UserId = groupUserId;
                        notification.IsRead = false;
                        notification.Message = "Korisnik " + caller.GetUserTitle() + " se odjavljuje iz sustava te se stoga prekida komunikacija!";

                        notificationRepository.Insert(notification);

                        User user = userRepository.GetById(groupUserId);

                        serverRequest = new ServerRequest();
                        serverRequest.Notification = notification;
                        serverRequest.User = user;
                        serverRequest.ServerRequestType = ServerRequestType.Notification;
                        serverRequest.UserControlId = Constants.NotificationControlId;

                        if (_sessionManager.ContainsUserId(groupUserId))
                        {
                            _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(groupUserId));
                        }
                    }
                }

                //pospremi razgovor
                Conversation conversation = conversationRepository.GetById(receivedConversation.Id);

                conversation.EndedOn = DateTime.Now;

                conversationRepository.Update(conversation);

                //pošalji zahtjev da se vrate i prikažu prikaz grupe
                serverRequest = new ServerRequest();

                foreach (int userId in groupUsers)
                {
                    User user = userRepository.GetById(userId);

                    serverRequest.User = user;
                    serverRequest.ServerRequestType = ServerRequestType.EndConversation;
                    serverRequest.UserControlId = Constants.GroupControlId;
                    serverRequest.Group = group;

                    if (_sessionManager.ContainsUserId(userId))
                    {
                        _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                    }
                }

                //obriši te zahtjeve iz baze
                List<Request> requests = new List<Request>();
                requests = requestRepository.GetRequestsForGroupOfType(group.Id,
                    (int)GroupRequestType.EndConversation).ToList();

                requestRepository.DeleteRequests(requests);
            }
        }

        public void ChangeApplicationParameters(ServerRequest serverRequest)
        {
            ApplicationParametersRepository applicationParametersRepository = new ApplicationParametersRepository();

            serverRequest.ApplicationParameters.UserId = serverRequest.User.Id;
            applicationParametersRepository.InsertOrUpdate(serverRequest.ApplicationParameters);
        }

        public void DeleteUser(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            userRepository.Delete(serverRequest.User);
        }

        public void UpdateUser(ServerRequest serverRequest)
        {
            try
            {
                UserRepository userRepository = new UserRepository();
                userRepository.Update(serverRequest.User);

                WineryRepository wineryRepository = new WineryRepository();
                wineryRepository.Update(serverRequest.Winery);

                //dodati brisanje i dodavanje vina

                WineRepository wineRepository = new WineRepository();
                wineRepository.SyncWines(serverRequest.Wines, serverRequest.Winery.Id);
            }
            catch(Exception ex)
            {
                serverRequest.Message = "Pogreška prilikom izmjene podataka korisnika.";
            }
        }

        public void GetBlockedUsers(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            serverRequest.Winemakers = userRepository.GetBlockedUsers(serverRequest.User.Id).ToList();

            SetUsersSignedInStatus(serverRequest);
        }

        public void SetUserLoggedInStatus(ServerRequest serverRequest, bool loggedIn)
        {
            UserRepository userRepository = new UserRepository();
            userRepository.SetUserLoggedInStatus(serverRequest.User.Id, loggedIn);
        }

        public void SetNotificationRead(ServerRequest serverRequest)
        {
            NotificationRepository notificationRepository = new NotificationRepository();
            notificationRepository.SetNotificationRead(serverRequest.NotificationReadId);
        }

        public void SetRequestAnswer(ServerRequest serverRequest)
        {
            RequestRepository requestRepository = new RequestRepository();
            GroupRepository groupRepository = new GroupRepository();
            UserRepository userRepository = new UserRepository();
            UserInGroupRepository userInGroupRepository = new UserInGroupRepository();
            NotificationRepository notificationRepository = new NotificationRepository();

            try
            {
                requestRepository.Update(serverRequest.RequestAnswer);
            }
            catch (Exception ex)
            {
                return;    
            }

            Group group = groupRepository.GetById(serverRequest.RequestAnswer.GroupID);

            if (serverRequest.RequestAnswer.RequestType == (int) GroupRequestType.CreateGroup)
            {
                #region Obrada zahtjeva vezanih uz stvaranje grupe
               
                HashSet<int> userIds = _sessionManager.GetGroupUsers(group.Id);

                if (serverRequest.RequestAnswer.Accepted == true)
                {
                    #region groupMightBeCreated

                    if (group.AreRequestsSatisfied(serverRequest.RequestAnswer.RequestType))
                    {
                        foreach (int userId in userIds)
                        {
                            User user = userRepository.GetById(userId);

                            Notification notification = new Notification();
                            notification.Message = "Grupa " + group.GroupName + " je uspješno kreirana!";
                            notification.UserId = userId;
                            notification.IsRead = false;

                            notificationRepository.Insert(notification);

                            serverRequest = new ServerRequest();
                            serverRequest.Notification = notification;
                            serverRequest.User = user;
                            serverRequest.ServerRequestType = ServerRequestType.Notification;
                            serverRequest.UserControlId = Constants.NotificationControlId;

                            if (_sessionManager.ContainsUserId(userId))
                            {
                                _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                            }

                            UserInGroup userInGroup = new UserInGroup();
                            userInGroup.UserId = userId;
                            userInGroup.GroupId = group.Id;

                            userInGroupRepository.Insert(userInGroup);
                        }

                        group.IsVisible = true;
                        groupRepository.Update(group);

                        serverRequest = new ServerRequest();
                        serverRequest.Group = group;

                        foreach (int userId in userIds)
                        {
                            User user = userRepository.GetById(userId);

                            serverRequest.User = user;
                            serverRequest.ServerRequestType = ServerRequestType.GroupCreated;
                            serverRequest.UserControlId = Constants.GroupControlId;

                            if (_sessionManager.ContainsUserId(userId))
                            {
                                _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                            }
                        }

                        //obriši te zahtjeve iz baze
                        List<Request> requests = new List<Request>();
                        requests = requestRepository.GetRequestsForGroupOfType(group.Id,
                            (int) GroupRequestType.CreateGroup).ToList();

                        requestRepository.DeleteRequests(requests);


                    }

                    #endregion
                }
                else
                {
                    #region groupNotCreated

                    foreach (int userId in userIds)
                    {
                        User user = userRepository.GetById(userId);

                        Notification notification = new Notification();
                        notification.Message = "Grupa " + group.GroupName + " nije kreirana jer je netko od pozvanih članova odbio zahtjev!";
                        notification.UserId = userId;
                        notification.IsRead = false;

                        notificationRepository.Insert(notification);

                        serverRequest = new ServerRequest();
                        serverRequest.Notification = notification;
                        serverRequest.User = user;
                        serverRequest.ServerRequestType = ServerRequestType.Notification;
                        serverRequest.UserControlId = Constants.NotificationControlId;

                        if (_sessionManager.ContainsUserId(userId))
                        {
                            _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else if (serverRequest.RequestAnswer.RequestType == (int) GroupRequestType.CloseGroup)
            {
                #region Obrada zahtjeva vezanih uz zatvaranje grupe

                List<int> userIds = userInGroupRepository.GetGroupUserIds(serverRequest.RequestAnswer.GroupID);

                if (serverRequest.RequestAnswer.Accepted == true)
                {
                    List<UserInGroup> userInGroups = new List<UserInGroup>();

                    if (group.AreRequestsSatisfied(serverRequest.RequestAnswer.RequestType))
                    {
                        //pošalji obavijest

                        foreach (int userId in userIds)
                        {
                            User user = userRepository.GetById(userId);
                            userInGroups.Add(userInGroupRepository.GetUserInGroup(userId, group.Id));

                            Notification notification = new Notification();
                            notification.Message = "Grupa " + group.GroupName + " je uspješno ukinuta!";
                            notification.UserId = userId;
                            notification.IsRead = false;

                            notificationRepository.Insert(notification);

                            serverRequest = new ServerRequest();
                            serverRequest.Notification = notification;
                            serverRequest.User = user;
                            serverRequest.ServerRequestType = ServerRequestType.Notification;
                            serverRequest.UserControlId = Constants.NotificationControlId;

                            if (_sessionManager.ContainsUserId(userId))
                            {
                                _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                            }
                        }

                        serverRequest = new ServerRequest();
                        serverRequest.Group = group;

                        //izbriši iz baze

                        foreach (UserInGroup userInGroup in userInGroups)
                        {
                            userInGroupRepository.Delete(userInGroup);
                            
                        }

                        groupRepository.Delete(group);

                        //makni grupu s popisa

                        foreach (int userId in userIds)
                        {
                            User user = userRepository.GetById(userId);

                            serverRequest.User = user;
                            serverRequest.ServerRequestType = ServerRequestType.GroupClosed;
                            serverRequest.UserControlId = Constants.GroupControlId;

                            if (_sessionManager.ContainsUserId(userId))
                            {
                                _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                            }
                        }

                        //obriši te zahtjeve iz baze
                        List<Request> requests = new List<Request>();
                        requests = requestRepository.GetRequestsForGroupOfType(group.Id,
                            (int)GroupRequestType.CloseGroup).ToList();

                        requestRepository.DeleteRequests(requests);
                    }
                }
                else
                {
                    foreach (int userId in userIds)
                    {
                        User user = userRepository.GetById(userId);

                        Notification notification = new Notification();
                        notification.Message = "Grupa " + group.GroupName + " nije uklonjena jer je netko od članova odbio zahtjev za njenim ukidanjem!";
                        notification.UserId = userId;
                        notification.IsRead = false;

                        notificationRepository.Insert(notification);

                        serverRequest = new ServerRequest();
                        serverRequest.Notification = notification;
                        serverRequest.User = user;
                        serverRequest.ServerRequestType = ServerRequestType.Notification;
                        serverRequest.UserControlId = Constants.NotificationControlId;

                        if (_sessionManager.ContainsUserId(userId))
                        {
                            _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                        }
                    }
                }
                #endregion
            }
            else if (serverRequest.RequestAnswer.RequestType == (int) GroupRequestType.EndConversation)
            {
                #region Obrada zahtjeva vezanih uz zatvaranje razgovora

                HashSet<int> userIds = _sessionManager.GetGroupUsers(group.Id);

                Request receivedRequest = serverRequest.RequestAnswer;

                if (serverRequest.RequestAnswer.Accepted == true)
                {
                    if (group.AreRequestsSatisfied(serverRequest.RequestAnswer.RequestType))
                    {
                        foreach (int userId in userIds)
                        {
                            User user = userRepository.GetById(userId);

                            Notification notification = new Notification();
                            notification.Message = "Grupni razgovor u grupi " + group.GroupName + " je uspješno završen!";
                            notification.UserId = userId;
                            notification.IsRead = false;

                            notificationRepository.Insert(notification);

                            serverRequest = new ServerRequest();
                            serverRequest.Notification = notification;
                            serverRequest.User = user;
                            serverRequest.ServerRequestType = ServerRequestType.Notification;
                            serverRequest.UserControlId = Constants.NotificationControlId;

                            if (_sessionManager.ContainsUserId(userId))
                            {
                                _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                            }
                        }

                        ConversationRepository conversationRepository = new ConversationRepository();

                        if (receivedRequest.ConversationId != null)
                        {
                            //pohrani zahtjev

                            Conversation conversation =
                                conversationRepository.GetById(receivedRequest.ConversationId.Value);

                            conversation = conversationRepository.GetById(receivedRequest.ConversationId.Value);
                            conversation.EndedOn = DateTime.Now;

                            conversationRepository.Update(conversation);

                            //pošalji zahtjev da se vrate i prikažu prikaz grupe
                            serverRequest = new ServerRequest();

                            foreach (int userId in userIds)
                            {
                                User user = userRepository.GetById(userId);

                                serverRequest.User = user;
                                serverRequest.ServerRequestType = ServerRequestType.EndConversation;
                                serverRequest.UserControlId = Constants.GroupControlId;
                                serverRequest.Group = group;

                                if (_sessionManager.ContainsUserId(userId))
                                {
                                    _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                                }
                            }
                        }

                        //obriši te zahtjeve iz baze
                        List<Request> requests = new List<Request>();
                        requests = requestRepository.GetRequestsForGroupOfType(group.Id,
                            (int)GroupRequestType.EndConversation).ToList();

                        requestRepository.DeleteRequests(requests);
                    }
                }
                else
                {
                    foreach (int userId in userIds)
                    {
                        User user = userRepository.GetById(userId);

                        Notification notification = new Notification();
                        notification.Message = "Grupni razgovor za grupu " + group.GroupName + " nije zatvoren jer je netko od članova odbio zahtjev za zatvaranjem!";
                        notification.UserId = userId;
                        notification.IsRead = false;

                        notificationRepository.Insert(notification);

                        serverRequest = new ServerRequest();
                        serverRequest.Notification = notification;
                        serverRequest.User = user;
                        serverRequest.ServerRequestType = ServerRequestType.Notification;
                        serverRequest.UserControlId = Constants.NotificationControlId;

                        if (_sessionManager.ContainsUserId(userId))
                        {
                            _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId));
                        }
                    }

                    //obriši te zahtjeve iz baze
                    List<Request> requests = new List<Request>();
                    requests = requestRepository.GetRequestsForGroupOfType(group.Id,
                        (int)GroupRequestType.EndConversation).ToList();

                    requestRepository.DeleteRequests(requests);
                }

                #endregion
            }
        }

        private void SetUsersSignedInStatus(ServerRequest serverRequest)
        {
            foreach (User winemaker in serverRequest.Winemakers)
            {
                if (_sessionManager.ContainsUserId(winemaker.Id))
                {
                    winemaker.SignedIn = true;
                }
                else
                {
                    winemaker.SignedIn = false;
                }
            }
        }

        private void SendGroupRequest(Request request, User user, int senderId)
        {
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.User = user;
            serverRequest.UserControlId = Constants.RequestControlId;
            serverRequest.SendRequest = request;
            serverRequest.SendRequest.RequestMessage = CommonMethods.GetRequestMessage(request);

            if (_sessionManager.ContainsUserId(user.Id))
            {
                if (user.Id != senderId)
                {
                    _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(request.UserId));
                }
            }
        }

        public void GetWineryAndWines(ServerRequest serverRequest)
        {
            WineRepository wineRepository = new WineRepository();
            WineryRepository wineryRepository = new WineryRepository();

            serverRequest.Winery = wineryRepository.GetUserWinery(serverRequest.UserId);
            if (serverRequest.Winery != null) { 
                serverRequest.Wines = wineRepository.GetWineryWines(serverRequest.Winery.Id);
            }
        }

        public void BlockUser(ServerRequest serverRequest)
        {
            if (serverRequest.BlockUserType == true)
            {
                try {
                    BlockedUsersRepository blockedUsersRepository = new BlockedUsersRepository();
                    BlockedUser blockedUser = new BlockedUser();
                    blockedUser.BlockingUserId = serverRequest.User.Id;
                    blockedUser.BlockedUserId = serverRequest.UserId;
                    blockedUsersRepository.Insert(blockedUser);

                    serverRequest.Message = "Korisnik je uspješno blokiran!";
                }
                catch (Exception ex)
                {
                    serverRequest.Message = "Korisnik je već blokiran!";
                }
            }
            else
            {
                try
                {
                    //TODO odblokiraj
                    BlockedUsersRepository blockedUsersRepository = new BlockedUsersRepository();
                    BlockedUser blockedUser = blockedUsersRepository.GetBlockedUserRecord(serverRequest.User.Id,
                        serverRequest.UserId);
                    blockedUsersRepository.Delete(blockedUser);

                    serverRequest.Message = "Korisnik je uspješno odblokiran!";
                }
                catch (Exception ex)
                {
                    serverRequest.Message = "Korisnici nisu bili blokirani!";
                }
            }
        }

        public void GetGroupUsers(ServerRequest serverRequest)
        {
            UserInGroupRepository userInGroupRepository = new UserInGroupRepository();
            UserRepository userRepository = new UserRepository();

            List<User> users = new List<User>();

            if (serverRequest.Group != null)
                foreach (int groupUserId in userInGroupRepository.GetGroupUserIds(serverRequest.Group.Id))
                {
                    users.Add(userRepository.GetById(groupUserId));
                }

            serverRequest.Winemakers = users;

            SetUsersSignedInStatus(serverRequest);
        }

        public void GetGroupLoggedIn(ServerRequest serverRequest)
        {
            UserInGroupRepository userInGroupRepository = new UserInGroupRepository();

            List<int> userIds = userInGroupRepository.GetGroupUserIds(serverRequest.Group.Id);

            bool allLoggedIn = userIds.All(userId => _sessionManager.ContainsUserId(userId));

            serverRequest.GroupLoggedIn = allLoggedIn;
        }

        public void StartConversation(ServerRequest serverRequest)
        {
            ConversationRepository conversationRepository = new ConversationRepository();
            Conversation conversation = conversationRepository.GetLastGroupConversation(serverRequest.Group.Id);

            if (conversation == null)
            {
                conversation = new Conversation();
                conversation.StartedOn = DateTime.Now;
                conversation.GroupId = serverRequest.Group.Id;
                conversationRepository.Insert(conversation);
            }
            else
            {
                serverRequest.Message = GetConversationMessages(conversation.Id);
            }

            serverRequest.Conversation = conversation;
            HashSet<int> groupUsers = GetGroupUsers(serverRequest.Group.Id);

            _sessionManager.AddGroup(serverRequest.Group.Id, groupUsers);
        }

        private HashSet<int> GetGroupUsers(int groupId)
        {
            UserRepository userRepository = new UserRepository();
            UserInGroupRepository userInGroupRepository = new UserInGroupRepository();

            return new HashSet<int>(userInGroupRepository.GetGroupUserIds(groupId));
        }

        public void SendMessage(ServerRequest serverRequest)
        {
            MessageRepository messageRepository = new MessageRepository();
            Message message = new Message();
            message.ConversationId = serverRequest.Conversation.Id;
            message.Content = serverRequest.Message;
            message.UserId = serverRequest.User.Id;
            message.SentOn = DateTime.Now;

            messageRepository.Insert(message);
        }

        public void LeaveGroup(ServerRequest serverRequest)
        {
            UserInGroupRepository userInGroupRepository = new UserInGroupRepository();
            UserRepository userRepository = new UserRepository();
            NotificationRepository notificationRepository = new NotificationRepository();

            UserInGroup userInGroup = userInGroupRepository.GetUserInGroup(serverRequest.User.Id, serverRequest.Group.Id);

            List<int> userIds = userInGroupRepository.GetGroupUserIds(serverRequest.Group.Id);

            //još je samo trenutačni korisnik ostao
            if (userIds.Count == 1)
            {
                GroupRepository groupRepository = new GroupRepository();
                groupRepository.Delete(serverRequest.Group);
            }

            foreach (int userId in userIds)
            {
                if (userId != serverRequest.User.Id)
                {
                    Notification notification = new Notification();
                    User user = userRepository.GetById(userId);
                    notification.UserId = userId;
                    notification.IsRead = false;
                    notification.Message = "Korisnik " + serverRequest.User.GetUserTitle() + " je napustio grupu " +
                                           serverRequest.Group.GroupName + ".";
                    
                    notificationRepository.Insert(notification);

                    //sad ih treba i poslati

                    SendNotification(user, notification);

                }
                else
                {
                    Notification notification = new Notification();
                    User user = userRepository.GetById(userId);
                    notification.UserId = userId;
                    notification.IsRead = false;
                    notification.Message = "Grupa " + serverRequest.Group.GroupName + " je uspješno napuštena.";

                    notificationRepository.Insert(notification);

                    SendNotification(user, notification);
                }
            }

            userInGroupRepository.Delete(userInGroup);
        }

        public void GetConversationHistory(ServerRequest serverRequest)
        {
            ConversationRepository conversationRepository = new ConversationRepository();
            List<Conversation> conversations =
                conversationRepository.GetGroupConversations(serverRequest.Group.Id).ToList();

            serverRequest.GroupConversations = conversations;
        }

        public void GetConversationMessages(ServerRequest serverRequest)
        {
            serverRequest.Message = GetConversationMessages(serverRequest.Conversation.Id);
        }

        public void SendDirectMessageOrEmail(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            User user = userRepository.GetById(serverRequest.UserId);

            BlockedUsersRepository blockedUsersRepository = new BlockedUsersRepository();

            try
            {
                blockedUsersRepository.GetBlockedUserRecord(serverRequest.User.Id, serverRequest.UserId);
                serverRequest.Message = "Poruka nije poslana. Korisnik blokira vas ili vi njega!";
            }
            catch (Exception)
            {
                try
                {
                    blockedUsersRepository.GetBlockedUserRecord(serverRequest.UserId, serverRequest.User.Id);
                    serverRequest.Message = "Poruka nije poslana. Korisnik blokira vas ili vi njega!";
                }
                catch (Exception)
                {
                    if (_sessionManager.ContainsUserId(serverRequest.UserId))
                    {
                        Notification notification = new Notification();
                        NotificationRepository notificationRepository = new NotificationRepository();

                        notification.UserId = serverRequest.UserId;
                        notification.IsRead = false;
                        notification.Message = "Korisnik " + serverRequest.User.UserName +
                                               " vam šalje sljedeću poruku: " +
                                               serverRequest.Message;
                        notificationRepository.Insert(notification);
                        SendNotification(user, notification);
                        serverRequest.Message = "Vaša poruka je uspješno poslana korisniku";
                    }
                    else
                    {
                        var fromAddress = new MailAddress("marincivinari@gmail.com", serverRequest.User.UserName);
                        var toAddress = new MailAddress(user.Email, user.UserName);
                        const string fromPassword = "l7MOjH5puKT7F5HoqT";
                        string subject = "Poruka od korisnika " + serverRequest.User.UserName;
                        string body = serverRequest.Message;

                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                            Timeout = 20000
                        };
                        using (var message = new MailMessage(fromAddress, toAddress)
                        {
                            Subject = subject,
                            Body = body
                        })
                        {
                            smtp.Send(message);
                        }

                        serverRequest.Message = "Vaša poruka je poslana korisniku putem e-maila";
                    }
                }
            }
           
        }

        public void CloseGroup(ServerRequest serverRequest)
        {
            SaveRequests(serverRequest.Group.Id, GroupRequestType.CloseGroup, serverRequest.User.Id);
        }

        private void SendNotification(User user, Notification notification)
        {
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.Notification = notification;
            serverRequest.User = user;
            serverRequest.ServerRequestType = ServerRequestType.Notification;
            serverRequest.UserControlId = Constants.NotificationControlId;

            if (_sessionManager.ContainsUserId(user.Id))
            {
                _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(user.Id));
            }
        }

        private string GetConversationMessages(int conversationId)
        {
            MessageRepository messageRepository = new MessageRepository();
            List<Message> messages = messageRepository.GetConversationMessages(conversationId).ToList();

            StringBuilder sb = new StringBuilder();

            foreach (Message message in messages)
            {
                sb.Append(message.Content);
                sb.AppendLine();
            }

            return sb.ToString();
        }

        public void EndConversation(ServerRequest serverRequest)
        {
            SaveRequests(serverRequest.Group.Id, GroupRequestType.EndConversation, serverRequest.User.Id, serverRequest.Conversation.Id);
        }
    }
}
