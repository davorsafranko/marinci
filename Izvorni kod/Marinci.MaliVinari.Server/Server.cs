﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Common;
using Marinci.MaliVinari.Business.Models;
using Newtonsoft.Json;

namespace Marinci.MaliVinari.Server
{
    public class Server
    {
        private readonly SessionManager _sessionManager;
        private volatile IPEndPoint _localIpEndPoint;
        private volatile ServerParameters _serverParameters;
        private readonly Dispatcher _dispatcher;
        private volatile bool _runServer = false;
        private volatile Socket _listenerSocket;
        private Thread _listenerThread;
        private const int BufferSize = 65536;

        public Server(string serverIpAddress, ServerParameters serverParameters)
        {
            _serverParameters = serverParameters;
            _sessionManager = new SessionManager(serverParameters.MaxNumberOfUsers);
            _dispatcher = new Dispatcher(_sessionManager, this);
            _localIpEndPoint = new IPEndPoint(IPAddress.Parse(serverIpAddress), serverParameters.Port);
        }

        public void StartServer()
        {
            _runServer = true;    
            _listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _listenerSocket.Bind(_localIpEndPoint);
            _listenerSocket.Listen(_serverParameters.MaxNumberOfUsers);

            _listenerThread = new Thread(Listen);
            _listenerThread.Start();
        }

        private void Listen()
        {
            while (_runServer)
            {
                Socket clientSocket = _listenerSocket.Accept();
                Console.WriteLine("Dobio zahtjev.");
                Task.Run(() => ProcessRequest(clientSocket));
            }
        }

        private void ProcessRequest(Socket clientSocket)
        {
            //iz socketa možemo saznati port s kojeg smo primili zahtjev, ali ne znamo port na kojem klijent sluša pristigle zahtjeve

            string receivedData = "";
            byte[] receiveBuffer = new byte[BufferSize];

            while (true)
            {
                int receivedBytes = clientSocket.Receive(receiveBuffer);
                receivedData += Encoding.UTF8.GetString(receiveBuffer, 0, receivedBytes);

                if (receivedBytes < BufferSize)
                {
                    break;
                }
            }

            ServerRequest serverRequest = JsonConvert.DeserializeObject<ServerRequest>(receivedData);

            if (serverRequest.User.Id != -1 && serverRequest.ServerRequestType != ServerRequestType.LogOut && serverRequest.ServerRequestType != ServerRequestType.ShutDownServer && serverRequest.ServerRequestType != ServerRequestType.ChangeServerParameters)
            {
                bool successful = _sessionManager.AddUserInfo(serverRequest.User.Id, serverRequest.IpAddress, serverRequest.ListeningPort);

                if (!successful)
                {
                    serverRequest.ServerRequestType = ServerRequestType.MaxUsersLimit;
                }
                else
                {
                    _dispatcher.SetUserLoggedInStatus(serverRequest, true);
                }
            }else if (serverRequest.User.Id == -1 && serverRequest.ServerRequestType == ServerRequestType.LogIn)
            {
                if (_sessionManager.GetNumberOfLoggedInUsers() == _serverParameters.MaxNumberOfUsers)
                {
                    serverRequest.ServerRequestType = ServerRequestType.MaxUsersLimit;
                }
            }

            //pročitaj iz zahtjeva što treba napraviti i napravi to

            DoAction(serverRequest);

            //vrati natrag
            if (serverRequest.ServerRequestType != ServerRequestType.LogOut && serverRequest.ServerRequestType != ServerRequestType.RequestAnswered && serverRequest.ServerRequestType != ServerRequestType.NotificationRead && serverRequest.ServerRequestType != ServerRequestType.Notification && serverRequest.ServerRequestType != ServerRequestType.ShutDownServer && serverRequest.ServerRequestType != ServerRequestType.ChangeServerParameters)
            {
                SendResponse(serverRequest);
            }

            clientSocket.Close();
        }

        private void SendResponse(ServerRequest serverRequest, bool respondToSender = true)
        {
            //Obradi zahtjev
            if (serverRequest.Conversation != null && serverRequest.ServerRequestType == ServerRequestType.SendMessage)
            {
                foreach (int userId in _sessionManager.GetGroupUsers(serverRequest.Conversation.GroupId))
                { 
                    Task.Run(() => SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(userId)));
                }
            }
            else
            {
                //login

                if (serverRequest.User != null && !_sessionManager.ContainsUserId(serverRequest.User.Id) && serverRequest.ServerRequestType != ServerRequestType.LogOut)
                {
                    _sessionManager.AddUserInfo(serverRequest.User.Id, serverRequest.IpAddress, serverRequest.ListeningPort);
                }

                if (serverRequest.User != null && serverRequest.ServerRequestType != ServerRequestType.LogOut && _sessionManager.ContainsUserId(serverRequest.User.Id))
                {
                    Task.Run(() => SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(serverRequest.User.Id)));
                }
                else
                {
                    Task.Run(() => SendResponseToClient(serverRequest, new IPEndPoint(IPAddress.Parse(serverRequest.IpAddress), serverRequest.ListeningPort)));
                }
            }
        }

        public void SendResponseToClient(ServerRequest serverRequest, IPEndPoint ipEndPoint)
        {
            Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            clientSocket.Connect(ipEndPoint);

            byte[] response = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(serverRequest));

            clientSocket.Send(response);

            clientSocket.Close();
        }

        private void DoAction(ServerRequest serverRequest)
        {
            switch (serverRequest.ServerRequestType)
            {
                    case ServerRequestType.LogIn: { _dispatcher.GetUserByUserName(serverRequest) ;break;}
                    case ServerRequestType.Register: { _dispatcher.RegisterUser(serverRequest); break;}
                    case ServerRequestType.CreateGroup: { _dispatcher.CreateGroup(serverRequest); break;}
                    case ServerRequestType.GetWinemakers: { _dispatcher.GetWinemakers(serverRequest); break;}
                    case ServerRequestType.ChangeApplicationParameters: { ChangeServerParameters(serverRequest); break;}
                    case ServerRequestType.ShutDownServer: { _dispatcher.ShutDownServer(serverRequest); break;}
                    case ServerRequestType.LogOut: { _dispatcher.LogOutUser(serverRequest); break;}
                    case ServerRequestType.DeleteUser: { _dispatcher.DeleteUser(serverRequest); break; }
                    case ServerRequestType.UpdateUser: { _dispatcher.UpdateUser(serverRequest); break; }
                    case ServerRequestType.GetBlockedUsers: { _dispatcher.GetBlockedUsers(serverRequest); break; }
                    case ServerRequestType.NotificationRead: { _dispatcher.SetNotificationRead(serverRequest); break; }
                    case ServerRequestType.RequestAnswered: { _dispatcher.SetRequestAnswer(serverRequest); break; }
                    case ServerRequestType.GetWineryAndWines: { _dispatcher.GetWineryAndWines(serverRequest); break; }
                    case ServerRequestType.BlockUser:{ _dispatcher.BlockUser(serverRequest); break; }
                    case ServerRequestType.GetGroupUsers: { _dispatcher.GetGroupUsers(serverRequest); break; }
                    case ServerRequestType.GroupLoggedIn: { _dispatcher.GetGroupLoggedIn(serverRequest); break; }
                    case ServerRequestType.StartConversation: { _dispatcher.StartConversation(serverRequest); break; }
                    case ServerRequestType.SendDirectMessage: { _dispatcher.SendDirectMessageOrEmail(serverRequest); break; }
                    case ServerRequestType.SendMessage: { _dispatcher.SendMessage(serverRequest); break; }
                    case ServerRequestType.LeaveGroup: { _dispatcher.LeaveGroup(serverRequest); break; }
                    case ServerRequestType.GetConversationHistory: { _dispatcher.GetConversationHistory(serverRequest); break; }
                    case ServerRequestType.GetConversationMessages: { _dispatcher.GetConversationMessages(serverRequest); break; }
                    case ServerRequestType.CloseGroup: { _dispatcher.CloseGroup(serverRequest); break;}
                    case ServerRequestType.EndConversation: { _dispatcher.EndConversation(serverRequest); break; }
            }
        }

        private void ChangeServerParameters(ServerRequest serverRequest)
        {
            if (serverRequest.ApplicationParameters.IpAddress != _localIpEndPoint.Address.ToString() &&
                serverRequest.ApplicationParameters.Port != _localIpEndPoint.Port)
            {
                _sessionManager.RemoveUserFromConnectedUsers(serverRequest.User.Id);
            }
            _dispatcher.ChangeApplicationParameters(serverRequest);
        }

        public void UpdateServerParameters(ServerParameters serverParameters)
        {
            _localIpEndPoint.Port = serverParameters.Port;
            _serverParameters.MaxNumberOfUsers = serverParameters.MaxNumberOfUsers;
        }
    }
}
