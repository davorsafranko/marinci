﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marinci.MaliVinari.Business;
using Marinci.MaliVinari.Business.Common;
using Marinci.MaliVinari.Business.Models;

namespace Marinci.MaliVinari.Server
{
    public class Service
    {
        private SessionManager _sessionManager;
        private Server _server;

        public Service(SessionManager sessionManager, Server server)
        {
            _sessionManager = sessionManager;
            _server = server;
        }

        public void GetUserByUserName(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            User user = userRepository.GetUserByUserName(serverRequest.User.UserName);

            serverRequest.User = user;

            if (user == null)
            {
                return;
            }

            WineryRepository wineryRepository = new WineryRepository();
            Winery winery = wineryRepository.GetUserWinery(user.Id);

            serverRequest.Winery = winery;

            if (winery != null)
            {
                List<Wine> wines = new List<Wine>();

                WineRepository wineRepository = new WineRepository();
                wines = wineRepository.GetWineryWines(winery.Id);

                serverRequest.Wines = wines;
            }
            else
            {
                serverRequest.Wines = null;
            }

            NotificationRepository notificationRepository = new NotificationRepository();
            serverRequest.Notifications = notificationRepository.GetUsersNotifications(serverRequest.User.Id);

            RequestRepository requestRepository = new RequestRepository();
            serverRequest.Requests = requestRepository.GetUsersRequests(serverRequest.User.Id);

            GroupRepository groupRepository = new GroupRepository();


            StringBuilder sb = new StringBuilder();
            foreach (Request request in serverRequest.Requests)
            {
                sb.Clear();
                sb.Append("Imate zahtjev za " +
                          requestRepository.GetRequestName(request,
                              request.RequestType) + " grupe " +
                          groupRepository.GetById(request.GroupID).GroupName + ".");
                sb.AppendLine();
                sb.Append("Prihvaćate li zahtjev?");
                request.RequestMessage = sb.ToString();
            }
        }

        public void RegisterUser(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            User user = serverRequest.User;
            
            if (userRepository.GetUserByUserName(user.UserName) == null &&
                userRepository.GetUserByEmail(user.Email) == null)
            {
                userRepository.Insert(user);
            }
            else
            {
                serverRequest.User = new User();
                serverRequest.Message = "Korisnički ime ili email su već zauzeti.";
                return;
            }

            if (serverRequest.Winery != null)
            {
                serverRequest.Winery.UserId = serverRequest.User.Id;

                WineryRepository wineryRepository = new WineryRepository();
                wineryRepository.Insert(serverRequest.Winery);

                if (serverRequest.Wines != null)
                {
                    WineRepository wineRepository = new WineRepository();

                    foreach (Wine wine in serverRequest.Wines)
                    {
                        wine.WineryId = serverRequest.Winery.Id;
                        wineRepository.Insert(wine);
                    }
                }
            }
        }

        public void CreateGroup(ServerRequest serverRequest)
        {
            if (serverRequest.GroupRequestModel.GroupRequestType == GroupRequestType.CreateGroup)
            {
                GroupRepository groupRepository = new GroupRepository();
                Group group = new Group();

                group.GroupName = serverRequest.GroupRequestModel.GroupName;
                group.IsGroupChat = serverRequest.GroupRequestModel.IsGroupChat;
                group.IsVisible = false;

                groupRepository.Insert(group);

                foreach (int userId in serverRequest.GroupRequestModel.UserIds)
                {
                    _sessionManager.AddUserToGroup(group.Id, userId);
                }

                serverRequest.GroupId = group.Id;

                SaveRequests(group.Id, GroupRequestType.CreateGroup);
            }
        }

        private void SaveRequests(int groupId, GroupRequestType groupRequestType)
        {
            RequestRepository requestRepository = new RequestRepository();
            //poslati zahtjeve za stvaranje grupe
            foreach (int userId in _sessionManager.GetGroupUsers(groupId))
            {
                Request request = new Request();
                request.GroupID = groupId;
                request.UserId = userId;
                request.IsRead = false;
                request.Accepted = false;
                request.RequestType = (int)groupRequestType;//0 je kreiranje grupe
                
                requestRepository.Insert(request);

                UserRepository userRepository = new UserRepository();
                User user = userRepository.GetById(userId);

                SendGroupRequest(request, user);
            }
        }

        public void GetWinemakers(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            if (String.IsNullOrEmpty(serverRequest.SearchText))
            {
                serverRequest.Winemakers = userRepository.GetAllWinemakers(serverRequest.User.Id);
            }
            else
            {
                serverRequest.Winemakers = userRepository.Search(serverRequest.SearchText).ToList();
            }

            SetUsersSignedInStatus(serverRequest);
            
        }

        public void ShutDownServer()
        {
            //poslati poruku svim korisnicima o prekidu rada poslužitelja
            Environment.Exit(1);
        }

        public void LogOutUser(ServerRequest serverRequest)
        {
            _sessionManager.RemoveUserFromConnectedUsers(serverRequest.User.Id);
            SetUserLoggedInStatus(serverRequest, false);
        }

        public void ChangeApplicationParameters(ServerRequest serverRequest)
        {
            ApplicationParametersRepository applicationParametersRepository = new ApplicationParametersRepository();

            serverRequest.ApplicationParameters.UserId = serverRequest.User.Id;
            applicationParametersRepository.InsertOrUpdate(serverRequest.ApplicationParameters);
        }

        public void DeleteUser(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            userRepository.Delete(serverRequest.User);
        }

        public void UpdateUser(ServerRequest serverRequest)
        {
            try
            {
                UserRepository userRepository = new UserRepository();
                userRepository.Update(serverRequest.User);

                WineryRepository wineryRepository = new WineryRepository();
                wineryRepository.Update(serverRequest.Winery);

                //dodati brisanje i dodavanje vina

                WineRepository wineRepository = new WineRepository();
                wineRepository.SyncWines(serverRequest.Wines, serverRequest.Winery.Id);
            }
            catch(Exception ex)
            {
                serverRequest.Message = "Pogreška prilikom izmjene podataka korisnika.";
            }
        }

        public void GetBlockedUsers(ServerRequest serverRequest)
        {
            UserRepository userRepository = new UserRepository();
            serverRequest.Winemakers = userRepository.GetBlockedUsers(serverRequest.User.Id).ToList();

            SetUsersSignedInStatus(serverRequest);
        }

        public void SetUserLoggedInStatus(ServerRequest serverRequest, bool loggedIn)
        {
            UserRepository userRepository = new UserRepository();
            userRepository.SetUserLoggedInStatus(serverRequest.User.Id, loggedIn);
        }

        public void SetNotificationRead(ServerRequest serverRequest)
        {
            NotificationRepository notificationRepository = new NotificationRepository();
            notificationRepository.SetNotificationRead(serverRequest.NotificationReadId);
        }

        public void SetRequestAnswer(ServerRequest serverRequest)
        {
            RequestRepository requestRepository = new RequestRepository();
            requestRepository.Update(serverRequest.RequestAnswer);

            //GroupRepository groupRepository = new GroupRepository();
            
        }

        private void SetUsersSignedInStatus(ServerRequest serverRequest)
        {
            foreach (User winemaker in serverRequest.Winemakers)
            {
                if (_sessionManager.ContainsUserId(winemaker.Id))
                {
                    winemaker.SignedIn = true;
                }
                else
                {
                    winemaker.SignedIn = false;
                }
            }
        }

        private void SendGroupRequest(Request request, User user)
        {
            if (request.RequestType == (int)GroupRequestType.CreateGroup)
            {
                ServerRequest serverRequest = new ServerRequest();
                serverRequest.User = user;
                serverRequest.UserControlId = Constants.RequestControlId;
                serverRequest.SendRequest = request;

                if (_sessionManager.ContainsUserId(user.Id))
                {
                    _server.SendResponseToClient(serverRequest, _sessionManager.GetUserEndPointInfo(request.UserId));
                }
            }
        }

        public void GetWineryAndWines(ServerRequest serverRequest)
        {
            WineRepository wineRepository = new WineRepository();
            WineryRepository wineryRepository = new WineryRepository();

            serverRequest.Winery = wineryRepository.GetUserWinery(serverRequest.UserId);
            if (serverRequest.Winery != null) { 
                serverRequest.Wines = wineRepository.GetWineryWines(serverRequest.Winery.Id);
            }
        }

        public void BlockUser(ServerRequest serverRequest)
        {
            try
            {
                BlockedUsersRepository blockedUsersRepository = new BlockedUsersRepository();
                BlockedUser blockedUser = new BlockedUser();
                blockedUser.BlockingUserId = serverRequest.User.Id;
                blockedUser.BlockedUserId = serverRequest.UserId;
                blockedUsersRepository.Insert(blockedUser);
            }
            catch
            {
                serverRequest.Message = "Greška";
            }
        }
    }
}
