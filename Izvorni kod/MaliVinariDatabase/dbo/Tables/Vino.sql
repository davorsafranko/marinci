﻿CREATE TABLE [dbo].[Vino] (
    [idVino]             INT            IDENTITY (1, 1) NOT NULL,
    [idVinarija]         INT            NOT NULL,
    [Naziv_Vina]         NVARCHAR (50)  NULL,
    [Slika_Vina]         NVARCHAR (256) NULL,
    CONSTRAINT [PK_Vino] PRIMARY KEY CLUSTERED ([idVino] ASC),
    CONSTRAINT [FK_Vino_Vinarija] FOREIGN KEY ([idVinarija]) REFERENCES [dbo].[Vinarija] ([idVinarija]) ON DELETE CASCADE
);







