﻿CREATE TABLE [dbo].[Parametri_aplikacije] (
    [idParametar] INT          IDENTITY (1, 1) NOT NULL,
    [idKorisnik]  INT          NOT NULL,
    [IP_Adresa]   VARCHAR (15) NOT NULL,
    [Port]        INT          NOT NULL,
    [Boja_Skin]   VARCHAR (50) NULL,
    CONSTRAINT [PK_Parametri_aplikacije_1] PRIMARY KEY CLUSTERED ([idParametar] ASC),
    CONSTRAINT [FK_Parametri_aplikacije_Korisnik] FOREIGN KEY ([idKorisnik]) REFERENCES [dbo].[Korisnik] ([idKorisnik]) ON DELETE CASCADE,
    UNIQUE NONCLUSTERED ([idKorisnik] ASC)
);





