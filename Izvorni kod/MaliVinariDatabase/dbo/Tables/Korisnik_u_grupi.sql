﻿CREATE TABLE [dbo].[Korisnik_u_grupi] (
    [idKorisnikUGrupi] INT IDENTITY (1, 1) NOT NULL,
    [idGrupa]          INT NOT NULL,
    [idKorisnik]       INT NOT NULL,
    CONSTRAINT [PK_Korisnik_u_grupi] PRIMARY KEY CLUSTERED ([idKorisnikUGrupi] ASC),
    CONSTRAINT [FK_Kug_Grupa] FOREIGN KEY ([idGrupa]) REFERENCES [dbo].[Grupa] ([idGrupa]) ON DELETE CASCADE,
    CONSTRAINT [FK_Kug_Korisnik] FOREIGN KEY ([idKorisnik]) REFERENCES [dbo].[Korisnik] ([idKorisnik]) ON DELETE CASCADE
);





