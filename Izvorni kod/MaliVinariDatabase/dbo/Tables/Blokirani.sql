﻿CREATE TABLE [dbo].[Blokirani] (
    [idBlokada]             INT IDENTITY (1, 1) NOT NULL,
    [idKorisnik]            INT NOT NULL,
    [idBlokiranogKorisnika] INT NOT NULL,
    CONSTRAINT [PK_Blokirani_1] PRIMARY KEY CLUSTERED ([idBlokada] ASC),
    CONSTRAINT [FK_Blokirani_Korisnik] FOREIGN KEY ([idBlokiranogKorisnika]) REFERENCES [dbo].[Korisnik] ([idKorisnik]),
    CONSTRAINT [FK_OnajKojiBlokira_Korisnik] FOREIGN KEY ([idKorisnik]) REFERENCES [dbo].[Korisnik] ([idKorisnik]),
    CONSTRAINT [UQ_Pair] UNIQUE NONCLUSTERED ([idKorisnik] ASC, [idBlokiranogKorisnika] ASC)
);



