﻿CREATE TABLE [dbo].[Zahtjev] (
    [idZahtjev]    INT IDENTITY (1, 1) NOT NULL,
    [idGrupa]      INT NOT NULL,
    [idKorisnik]   INT NOT NULL,
    [Tip_Zahtjeva] INT NOT NULL,
    [Prihvacen]    BIT NOT NULL,
    [Odgovoren]    BIT NOT NULL,
    [idRazgovor]   INT NULL,
    CONSTRAINT [PK_Zahtjev] PRIMARY KEY CLUSTERED ([idZahtjev] ASC),
    CONSTRAINT [FK_Zahtjev_Grupa] FOREIGN KEY ([idGrupa]) REFERENCES [dbo].[Grupa] ([idGrupa]) ON DELETE CASCADE,
    CONSTRAINT [FK_Zahtjev_Korisnik] FOREIGN KEY ([idKorisnik]) REFERENCES [dbo].[Korisnik] ([idKorisnik]) ON DELETE CASCADE
);









