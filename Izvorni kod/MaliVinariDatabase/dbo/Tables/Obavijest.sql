﻿CREATE TABLE [dbo].[Obavijest] (
    [idObavijest]  INT            IDENTITY (1, 1) NOT NULL,
    [idKorisnik]   INT            NOT NULL,
    [Sadrzaj]      NVARCHAR (256) NULL,
    [Je_Procitana] BIT            NOT NULL,
    CONSTRAINT [PK_Obavijest] PRIMARY KEY CLUSTERED ([idObavijest] ASC),
    CONSTRAINT [FK_Obavijest_Korisnik] FOREIGN KEY ([idKorisnik]) REFERENCES [dbo].[Korisnik] ([idKorisnik]) ON DELETE CASCADE
);



