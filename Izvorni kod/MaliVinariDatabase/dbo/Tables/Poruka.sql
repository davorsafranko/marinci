﻿CREATE TABLE [dbo].[Poruka] (
    [idPoruka]   INT             IDENTITY (1, 1) NOT NULL,
    [idRazgovor] INT             NOT NULL,
    [idKorisnik] INT             NOT NULL,
    [Vrijeme]    DATETIME        NOT NULL,
    [Sadrzaj]    NVARCHAR (1024) NOT NULL,
    CONSTRAINT [PK_Poruka] PRIMARY KEY CLUSTERED ([idPoruka] ASC),
    CONSTRAINT [FK_Poruka_Razgovor] FOREIGN KEY ([idRazgovor]) REFERENCES [dbo].[Razgovor] ([idRazgovor]) ON DELETE CASCADE
);



