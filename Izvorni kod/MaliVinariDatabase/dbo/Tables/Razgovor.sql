﻿CREATE TABLE [dbo].[Razgovor] (
    [idRazgovor]        INT      IDENTITY (1, 1) NOT NULL,
    [Vrijeme_Pocetka]   DATETIME NOT NULL,
    [idGrupa]           INT      NOT NULL,
    [Vrijeme_Zavrsetka] DATETIME NULL,
    CONSTRAINT [PK_Razgovor_1] PRIMARY KEY CLUSTERED ([idRazgovor] ASC),
    CONSTRAINT [FK_Razgovor_Grupa] FOREIGN KEY ([idGrupa]) REFERENCES [dbo].[Grupa] ([idGrupa]) ON DELETE CASCADE
);

