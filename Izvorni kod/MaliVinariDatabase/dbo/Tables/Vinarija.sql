﻿CREATE TABLE [dbo].[Vinarija] (
    [idVinarija]      INT           IDENTITY (1, 1) NOT NULL,
    [idKorisnik]      INT           NOT NULL,
    [Naziv_Vinarije]  NVARCHAR (50) NULL,
    [Adresa_Vinarije] NVARCHAR (50) NULL,
    CONSTRAINT [PK_Vinarija_1] PRIMARY KEY CLUSTERED ([idVinarija] ASC),
    CONSTRAINT [FK_Vinarija_Korisnik] FOREIGN KEY ([idKorisnik]) REFERENCES [dbo].[Korisnik] ([idKorisnik]) ON DELETE CASCADE,
    UNIQUE NONCLUSTERED ([idKorisnik] ASC)
);









