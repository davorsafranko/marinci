﻿CREATE TABLE [dbo].[Korisnik] (
    [idKorisnik]     INT            IDENTITY (1, 1) NOT NULL,
    [Korisnicko_Ime] VARCHAR (50)   NOT NULL,
    [Email]          VARCHAR (256)  NOT NULL,
    [Lozinka]        VARCHAR (128)  NOT NULL,
    [Ime]            NVARCHAR (50)  NULL,
    [Prezime]        NVARCHAR (50)  NULL,
    [Osobni_Status]  NVARCHAR (256) NULL,
    [Profilna_Slika] VARCHAR (256)  NULL,
    [Telefon]        VARCHAR (50)   NULL,
    [Prijavljen]     BIT            NULL,
    [idUloga]        INT            NULL,
    CONSTRAINT [PK_Korisnik] PRIMARY KEY CLUSTERED ([idKorisnik] ASC),
    CONSTRAINT [ProvjeriIdUloge] CHECK ([idUloga]=(1) OR [idUloga]=(0)),
    CONSTRAINT [UQ_Email] UNIQUE NONCLUSTERED ([Email] ASC),
    CONSTRAINT [UQ_KorisnickoIme] UNIQUE NONCLUSTERED ([Korisnicko_Ime] ASC)
);









