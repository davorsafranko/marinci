BLOKIRANI
  korisnik_idKorisnik INT - id korisnika koji blokira
  blokiraniKorisnik_idKorisnik INT - id korisnika koji je blokiran
  K={korisnik_idKorisnik, blokiraniKorisnik_idKorisnik)

ULOGA
  idUloga INT - id uloge
  uloga VARCHAR(45) - definira je li korisnik administrator ili samo korisnik
  K={idUloga}

VINARIJA
  idVinarija INT - id pojedine vinarije
  naziv_vinarije VARCHAR(45) - sadrži naziv pojedine vinarije
  adresa_vinarije VARCHAR(45) - adresa na kojoj se nalazi vinarija
  slika_vinarije VARCHAR(45) - put do slike vinarije
  korisnik_idKorisnik INT - id korisnika vlasnika vinarije
  K={idVinarija, korisnik_idKorisnik}
 
  